<?php
	$page = "just-in";
	include('header.php');
?>


<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content">
				<div class="section flv_sections_26">

					<div class="section flv_sections_18">
						<div class="section_wrapper clearfix">
							<div class="items_group clearfix">
								<!-- Page Title-->
								<div class="column one column_fancy_heading">
									<div class="fancy_heading fancy_heading_line" style="padding-top:80px">
										<h2 class="title">This Just In</h2>
									</div>
								</div>
								
								<!-- One full width row-->
								<div class="column one column_blog">
									<div class="blog_wrapper isotope_wrapper clearfix">
									
									<!-- Masonry blog posts -->
									<div class="posts_group lm_wrapper masonry isotope">
										<div class="post-175 post type-post status-publish format-standard has-post-thumbnail hentry category-motion category-photography category-uncategorized tag-eclipse tag-grid tag-mysql post-item isotope-item clearfix author-admin">
											<div class="date_label">Jan. 13, 2017</div>
											
											<div class="image_frame post-photo-wrapper scale-with-grid">
												<div class="image_wrapper">
													<a href="blog-single-content-builder.html">
														<div class="mask"></div>
														<img width="576" height="450" src="images/beauty_portfolio_2-576x450.jpg" class="scale-with-grid wp-post-image" alt="beauty_portfolio_2" />
													</a>
													
													<div class="image_links double">
													<a href="images/beauty_portfolio_2.jpg" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="#" class="link"><i class="icon-link"></i></a>
												</div>
											</div>
										</div>

										<div class="post-desc-wrapper">
											<div class="post-desc">
												<div class="post-meta clearfix">
													<div class="author-date">
														<span class="author"><span>Published by </span><i class="icon-user"></i> <a href="#">James Shanks</a>
</span><span class="date"><span>  Jan 13, 2017</span>
													</div>
												</div>
												
												<div class="post-title">
													<h3 class="entry-title"><a href="#l">Investment Appreciation</a></h3>
												</div>

												<div class="post-excerpt">
													<span class="big">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac ele molestie id viverra aifend ante lobortis id. In viverra ipsum stie id viverra a.</span>
												</div>
												
												<div class="post-footer">
													<div class="post-links">
														<i class="icon-doc-text"></i><a href="#" class="post-more">Read more</a>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="post-186 post type-post status-publish format-image has-post-thumbnail hentry category-mobile category-photography category-uncategorized tag-design tag-html5 tag-wordpress post-item isotope-item clearfix author-admin">
										<div class="image_frame post-photo-wrapper scale-with-grid">
											<div class="image_wrapper">
												<a href="blog-single-vertical-photo.html">
													<div class="mask"></div>
													<img width="500" height="649" src="images/blog_vertical.jpg" class="scale-with-grid wp-post-image" alt="blog_vertical" />
												</a>
												
												<div class="image_links double">
													<a href="images/blog_vertical.jpg" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="#" class="link"><i class="icon-link"></i></a>
												</div>
											</div>
										</div>

										<div class="post-desc-wrapper">
											<div class="post-desc">
												<div class="post-meta clearfix">
													<div class="author-date">
														<span class="author"><span>Published by </span><i class="icon-user"></i> <a href="#">Amanda Speed</a>
</span><span class="date"><span> Dec. 11, 2016</span>
													</div>
												</div>

												<div class="post-title">
													<h3 class="entry-title"><a href="#">Post with vertical photo</a></h3>
												</div>
												
												<div class="post-excerpt">
													<span class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet cong ue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend.</span>
												</div>
												
												<div class="post-footer">
													<div class="post-links">
														<i class="icon-doc-text"></i><a href="blog-single-vertical-photo.html" class="post-more">Read more</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="post-175 post type-post status-publish format-standard has-post-thumbnail hentry category-motion category-photography category-uncategorized tag-eclipse tag-grid tag-mysql post-item isotope-item clearfix author-admin">
<div class="date_label">Jan. 13, 2017</div>
									
									<div class="image_frame post-photo-wrapper scale-with-grid">
										<div class="image_wrapper">
											<a href="blog-single-content-builder.html">
												<div class="mask"></div>
												<img width="576" height="450" src="images/beauty_portfolio_2-576x450.jpg" class="scale-with-grid wp-post-image" alt="beauty_portfolio_2" />
											</a>
											
											<div class="image_links double">
												<a href="images/beauty_portfolio_2.jpg" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="#" class="link"><i class="icon-link"></i></a>
											</div>
										</div>
									</div>
									
									<div class="post-desc-wrapper">
										<div class="post-desc">	
											<div class="post-meta clearfix">
												<div class="author-date">
													<span class="author"><span>Published by </span><i class="icon-user"></i> <a href="#">James Shanks</a>
</span><span class="date"><span>  Jan 13, 2017</span>
												</div>
											</div>

											<div class="post-title">
												<h3 class="entry-title"><a href="#l">Investment Appreciation</a></h3>
											</div>
											
											<div class="post-excerpt">
												<span class="big">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac ele molestie id viverra aifend ante lobortis id. In viverra ipsum stie id viverra a.</span>
											</div>
											
											<div class="post-footer">
												<div class="post-links">
													<i class="icon-doc-text"></i><a href="#" class="post-more">Read more</a>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="post-181 post type-post status-publish format-video has-post-thumbnail hentry category-motion category-photography category-uncategorized tag-eclipse tag-framework tag-wordpress post-item isotope-item clearfix author-admin">
									<div class="image_frame post-photo-wrapper scale-with-grid">
										<div class="image_wrapper">
											<iframe class="scale-with-grid" src="http://player.vimeo.com/video/32835557" allowFullScreen></iframe>
										</div>
									</div>

									<div class="post-desc-wrapper">
										<div class="post-desc">
											<div class="post-meta clearfix">
												<div class="author-date">
													<span class="author"><span>Published by </span><i class="icon-user"></i> <a href="#">James Shanks</a>
</span><span class="date"><span> at </span><i class="icon-clock"></i> Nov. 30, 2016</span>
												</div>
											</div>
											
											<div class="post-title">
												<h3 class="entry-title"><a href="blog-single-video.html">Video post with youtube / vimeo</a></h3>
											</div>
											
											<div class="post-excerpt">
												<span class="big">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac ele molestie id viverra aifend ante lobortis id. In viverra ipsum stie id viverra a.</span>
											</div>
											
											<div class="post-footer">
												<div class="post-links">
													<a href="blog-single-video.html" class="post-more">Read more</a>
												</div>
											</div>
										</div>
									</div>	
								</div>
								
								<div class="post-175 post type-post status-publish format-standard has-post-thumbnail hentry category-motion category-photography category-uncategorized tag-eclipse tag-grid tag-mysql post-item isotope-item clearfix author-admin">
<div class="date_label">Jan. 13, 2017</div>
								<div class="image_frame post-photo-wrapper scale-with-grid">
									<div class="image_wrapper">
										<a href="blog-single-content-builder.html">
											<div class="mask"></div>
											<img width="576" height="450" src="images/beauty_portfolio_2-576x450.jpg" class="scale-with-grid wp-post-image" alt="beauty_portfolio_2" />
										</a>
										
										<div class="image_links double">
											<a href="images/beauty_portfolio_2.jpg" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="#" class="link"><i class="icon-link"></i></a>
										</div>
									</div>
								</div>
								
								<div class="post-desc-wrapper">
									<div class="post-desc">
										<div class="post-meta clearfix">
											<div class="author-date">
												<span class="author"><span>Published by </span><i class="icon-user"></i> <a href="#">James Shanks</a>
</span><span class="date"><span>  Jan 13, 2017</span>
											</div>
										</div>

										<div class="post-title">
											<h3 class="entry-title"><a href="#l">Investment Appreciation</a></h3>
										</div>
										
										<div class="post-excerpt">
											<span class="big">Vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac ele molestie id viverra aifend ante lobortis id. In viverra ipsum stie id viverra a.</span>
										</div>
										
										<div class="post-footer">
											<div class="post-links">	
												<i class="icon-doc-text"></i><a href="#" class="post-more">Read more</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section flv_sections_25">
		<div class="section_wrapper clearfix">
			<div class="items_group clearfix"></div>
			
			<div class="section flv_sections_13">
				<div class="section_wrapper clearfix">
					<div class="items_group clearfix">

					<!-- Page Title-->
					<div class="column one column_fancy_heading">
						<div class="fancy_heading fancy_heading_line">
							<span class="slogan">Meet our contributors</span>
						</div>
					</div>
					
					<!-- One Second (1/2) Column -->
					<div class="column one-second column_our_team">
						<!-- Team Member Area -->
						<div class="team team_horizontal">
							<div class="image_frame no_link scale-with-grid">
								<div class="image_wrapper">
									<img class="scale-with-grid" src="images/James-Shanks.jpg" alt="James Shanks" />
								</div>
							</div>
							
							<div class="desc_wrapper">
								<h4>James A. Shanks</h4>
								<p class="subtitle">Partner</p>
								<hr class="hr_color" />
								
								<div class="desc">Jamie joined the company in 2007 and is responsible for real estate screening and due diligence as well as managing the acquisition and disposition processes.<br/><a href="#">View Jamie's Full Bio</a></div>
								<div class="links">
									<a href="mailto:noreply@envato.com" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
								</div>
							</div>
						</div>
					</div>

					<!-- One Second (1/2) Column -->
					<div class="column one-second column_our_team">
						<!-- Team Member Area -->
						<div class="team team_horizontal">
							<div class="image_frame no_link scale-with-grid">
								<div class="image_wrapper"><img class="scale-with-grid" src="images/Amanda-Speed.jpg" alt="Amanda Speed" /></div>
							</div>

							<div class="desc_wrapper">
								<h4>Amanda V. Speed</h4>
								<p class="subtitle">CPA, Partner</p>
								<hr class="hr_color" />
								<div class="desc">Amanda joined the company in 1994 and is responsible for accounting, budgeting, treasury management,  investor reporting and ensuring compliance with tax and audit requirements.<br/><a href="#">View Amanda's Full Bio</a></div>
								
								<div class="links">
									<a href="mailto:noreply@envato.com" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>


<?php include('footer.php'); ?>