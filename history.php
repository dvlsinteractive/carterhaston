<?php
	$page = "history";
	include('header.php');
?>



<div id="Content">
	<div class="content_wrapper clearfix">
		
		<div class="sections_group">
			<div class="entry-content">
				<div class="section flv_sections_11">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
						
						
						<!-- Page Title-->
						<div class="column one column_fancy_heading">
							<div class="fancy_heading" style="padding-top:80px">
								<h2 class="title">History of Success</h2>
							</div>
							
							<div class="column mcb-column one column_divider ">
								<hr class="" style="margin: 0 auto 10px auto;" />
							</div>
						</div>

							
						<div class="yearLinks">
							<p>Jump to:</p>
								<ul>
									<li><a href="#1984">1984</a></li>
									<li><a href="#1991">1991</a></li>
									<li><a href="#1998">1998</a></li>
									<li><a href="#1999">1999</a></li>
									<li><a href="#2002">2002</a></li>
									<li><a href="#2007">2007</a></li>
									<li><a href="#2009">2009</a></li>
									<li><a href="#2010">2010</a></li>
									<li><a href="#2013">2013</a></li>
									<li><a href="#2014">2014</a></li>
								</ul>
							</div>
							
						<div class="timeline">
							<div class="line"></div>
							
							<ul>
								<li id="1984">
									<span class="marker">1984</span>
									<h2>Third-Party Consulting, Management and Brokerage</h2>
									<div class="image border shadow">
										<img src="images/timeline-cropped/1984.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="1991">
									<span class="marker">1991</span>
									<h2>Launch of Carter-Haston Partnership Acquisitions</h2>
									<p>Carter-Haston begins to acquire apartment communities using single-asset, individually capitalized partnerships, which are now referred to as the Legacy Partnership.</p>
									<div class="image border shadow">
										<img src="images/timeline-cropped/1991.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="1998">
									<span class="marker">1998</span>
									<h2>Proposed Investment in Carter-Haston by Major University Endowment</h2>
									<p>This transaction was never completed as a result of changes in the macro economic environment at the time. However, the exercise of preparing for the transaction was incredibly valuable and left Carter-Haston as a more sophisticated real estate owner and operator and better able to report to its partners regarding the performance of its assets.</p>
									<div class="image border shadow">
										<img src="images/timeline-cropped/1998.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">1998</span>
									<h2>Senior Management Team Assembled</h2>
									<p><strong>Amanda Speed</strong> - Joined 1994 <br /><strong>Michael Fielder</strong> - Joined 1996 <br /><strong>John Carter</strong> - Joined 1998</p>
									<div class="image">
										<img src="images/timeline-cropped/1998-mgmt.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="1999">
									<span class="marker">1999</span>
									<h2>Disposition of Properties Begins</h2>
									<p>Carter-Haston sells its first four assets. Cumulative average annualized returns are greater than 30% on these sales.</p>
									<div class="image border shadow">
										<img src="images/timeline-cropped/1999.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="2002">
									<span class="marker">2002</span>
									<h2>Portfolio Grows from 1,500 to 6,200 Units</h2>
									<p>Carter-Haston was a net buyer of apartments until 2002 when the portfolio reached 6,200 units and approached $300 million in total assets.</p>
									<div class="image border shadow">
										<img src="images/timeline-cropped/2002.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="2007">
									<span class="marker">2007</span>
									<h2>Dispositions Accelerate through December 2007</h2>
									<p>Carter-Haston was a net seller of apartments in the mid-2000s in the face of increasing prices and competition for assets. During this time, company sold nearly all of its Florida portfolio to condominum converters for significant gains.</p>	
									<div class="image border shadow">
										<img src="images/timeline-cropped/2007-graph.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">2007</span>
									<h2>Senior Management Team Fully Assembled</h2>
									<p><strong>Amanda Speed</strong> - Joined 1998<br>
										<strong>Michael Fielder</strong> - Joined 1996<br>
										<strong>John Carter</strong> - Joined 1998<br>
										<strong>Jamie Shanks</strong> - Joined 2007
									</p>	
									<div class="image">
										<img src="images/timeline-cropped/2007-mgmt.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="2009">
									<span class="marker">2009</span>
									<h2>First Fund Launched: Carter-Haston Investment Partners I</h2>
									<div class="image">
										<img src="images/timeline-cropped/chips1.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">2009</span>
									<h2>$25 Million in Discretionary Equity, 1,347 Units Acquired</h2>
									<div class="image">
										<img src="images/timeline-cropped/2009-chips1-properties.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="2010">
									<span class="marker">2010</span>
									<h2>Second Fund Launched: Carter-Haston Investment Partners II</h2>
									<div class="image">
										<img src="images/timeline-cropped/chips2.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">2010</span>
									<h2>$44 Million in Discretionary Equity, 1,347 Units Acquired</h2>
									<div class="image">
										<img src="images/timeline-cropped/2010-chips2-properties.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="2013">
									<span class="marker">August 2013</span>
									<h2>Total Dispositions Reach $430 Million</h2>
									<div class="image">
										<img src="images/timeline-cropped/2013-aug.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">September 2013</span>
									<h2>Total Acquisitions Reach $1 Billion</h2>
									<div class="image">
										<img src="images/timeline-cropped/2013-sept.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">October 2013</span>
									<h2>$300 Million in Cumulative Equity Raised</h2>
									<div class="image">
										<img src="images/timeline-cropped/2013-oct.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li id="2014">
									<span class="marker">January 2014</span>
									<h2>Total Distributions Exceed $250 Million</h2>
									<div class="image">
										<img src="images/timeline-cropped/2014-jan.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>
								
								<li>
									<span class="marker">2014</span>
									<h2>Third Fund Launched: Carter-Haston Investment Partners III</h2>
									<div class="image">
										<img src="images/timeline-cropped/chips3.jpg" />
									</div>
									<div class="clearfix"></div>
								</li>								
							</ul>
						</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php include('footer.php'); ?>