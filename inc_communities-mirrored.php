<div class="section flv_sections_11">
				<div class="section_wrapper clearfix">
					<div class="items_group clearfix">

						<!-- One Fourth (1/4) Column -->
						<div class="communityFilter">
							<div class="column_attr ">
								<h4 class="themecolor">Search Communities</h4>
								<h6>FILTER YOUR RESULTS BY:</h6>
								
								<form method="post" action="" id="communityFrm">
									<span class="custom-select">
									<select id="communityChange">
										<option>Community</option>								
																				<option value="http://23.235.205.152/~chadmin/location/ansley-commons/">Ansley Commons</option>
																				<option value="http://23.235.205.152/~chadmin/location/anthem-cityline/">Anthem Cityline</option>
																				<option value="http://23.235.205.152/~chadmin/location/arbors-harbor-town/">Arbors Harbor Town</option>
																				<option value="http://23.235.205.152/~chadmin/location/barclay-at-dunwoody/">Barclay at Dunwoody</option>
																				<option value="http://23.235.205.152/~chadmin/location/bay-oaks-2/">Bay Oaks</option>
																				<option value="http://23.235.205.152/~chadmin/location/block-lofts/">Block Lofts</option>
																				<option value="http://23.235.205.152/~chadmin/location/brookleigh-flats/">Brookleigh Flats</option>
																				<option value="http://23.235.205.152/~chadmin/location/courtney-trace/">Courtney Trace</option>
																				<option value="http://23.235.205.152/~chadmin/location/cumberland-pointe-apartments/">Cumberland Pointe Apartments</option>
																				<option value="http://23.235.205.152/~chadmin/location/eastside-village/">Eastside Village</option>
																				<option value="http://23.235.205.152/~chadmin/location/glenwood-east/">Glenwood East</option>
																				<option value="http://23.235.205.152/~chadmin/location/grand-reserve/">Grand Reserve</option>
																				<option value="http://23.235.205.152/~chadmin/location/hunters-creek/">Hunters Creek</option>
																				<option value="http://23.235.205.152/~chadmin/location/lane-parke/">Lane Parke</option>
																				<option value="http://23.235.205.152/~chadmin/location/legacy-at-meridian/">Legacy at Meridian</option>
																				<option value="http://23.235.205.152/~chadmin/location/legacy-at-wakefield/">Legacy at Wakefield</option>
																				<option value="http://23.235.205.152/~chadmin/location/legacy-on-the-bay/">Legacy on the Bay</option>
																				<option value="http://23.235.205.152/~chadmin/location/lofts-at-savannah-park/">Lofts at Savannah Park</option>
																				<option value="http://23.235.205.152/~chadmin/location/marsh-point/">Marsh Point</option>
																				<option value="http://23.235.205.152/~chadmin/location/preserve-at-spears-creek/">Preserve at Spears Creek</option>
																				<option value="http://23.235.205.152/~chadmin/location/preserve-at-steele-creek/">Preserve at Steele Creek</option>
																				<option value="http://23.235.205.152/~chadmin/location/riverhouse/">Riverhouse</option>
																				<option value="http://23.235.205.152/~chadmin/location/san-miguel-del-bosque/">San Miguel Del Bosque</option>
																				<option value="http://23.235.205.152/~chadmin/location/station-92/">Station 92</option>
																				<option value="http://23.235.205.152/~chadmin/location/the-addison/">The Addison</option>
																				<option value="http://23.235.205.152/~chadmin/location/the-addison-at-south-tryon/">The Addison at South Tryon</option>
																				<option value="http://23.235.205.152/~chadmin/location/the-preserve-at-windsor-lake/">The Preserve at Windsor Lake</option>
																				<option value="http://23.235.205.152/~chadmin/location/the-q-at-maitland/">The Q at Maitland</option>
																				<option value="http://23.235.205.152/~chadmin/location/the-retreat-at-panama-city/">The Retreat at Panama City</option>
																				<option value="http://23.235.205.152/~chadmin/location/tradition-at-summerville/">Tradition at Summerville</option>
																				<option value="http://23.235.205.152/~chadmin/location/verandahs-of-brighton-bay/">Verandahs of Brighton Bay</option>
																				<option value="http://23.235.205.152/~chadmin/location/west-end-village/">West End Village</option>
																				<option value="http://23.235.205.152/~chadmin/location/whitepalm/">Whitepalm</option>
																			</select>
									</span>
									
									<p>or</p>
									
									<span class="custom-select">
									<select id="SearchState" name="SearchState">
										<option value="">State</option>
										<option value="AL">AL</option><option value="AR">AR</option><option value="FL">FL</option><option value="GA">GA</option><option value="IN">IN</option><option value="NC">NC</option><option value="NM">NM</option><option value="SC">SC</option><option value="TN">TN</option><option value="TX">TX</option>									</select>
									</span>
									
									
									<span class="custom-select">
									<select id="SearchCity" name="SearchCity" disabled="">
										<option value="">City</option>
																			</select>
									</span>
									
									<button type="submit" class="search">Search</button>
									<button type="reset" class="reset">Reset</button>
								</form>
															
							</div>
						</div>
						
						
						
<!-- FILTERED RESULTS -->
											
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/ansley-commons-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Ansley Commons</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">3300 Shipley Street N.</span><br>
							    <span itemprop="addressLocality">Charleston</span>,
							    <span itemprop="addressRegion">SC</span>
							    <span itemprop="postalCode">29456</span>
							  </div>
								<div itemprop="telephone">843.297.8316</div>
								<a href="http://www.ansleycommons.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/ansley-commons/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/anthem-cityline-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Anthem Cityline</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1250 State Street</span><br>
							    <span itemprop="addressLocality">Richardson</span>,
							    <span itemprop="addressRegion">TX</span>
							    <span itemprop="postalCode">75082</span>
							  </div>
								<div itemprop="telephone">972.231.2800</div>
								<a href="https://www.anthemcityline.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/anthem-cityline/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/arbors-harbor-town-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Arbors Harbor Town</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">671 Harbor Edge Drive</span><br>
							    <span itemprop="addressLocality">Memphis</span>,
							    <span itemprop="addressRegion">TN</span>
							    <span itemprop="postalCode">38103</span>
							  </div>
								<div itemprop="telephone">901.526.0322</div>
								<a href="http://www.arborsharbortown.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/arbors-harbor-town/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/barclay-dunwoody-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Barclay at Dunwoody</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">4580 Barclay Drive</span><br>
							    <span itemprop="addressLocality">Dunwoody</span>,
							    <span itemprop="addressRegion">GA</span>
							    <span itemprop="postalCode">30338</span>
							  </div>
								<div itemprop="telephone">770.986.9094</div>
								<a href="http://www.barclaydunwoody.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/barclay-at-dunwoody/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/bay-oaks-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Bay Oaks</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">3105 Bay Oaks Court</span><br>
							    <span itemprop="addressLocality">Tampa</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">33629</span>
							  </div>
								<div itemprop="telephone">813.839.8496</div>
								<a href="http://www.livingonbayshore.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/bay-oaks-2/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/block-lofts-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Block Lofts</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">747 Ralph McGill Blvd NE</span><br>
							    <span itemprop="addressLocality">Atlanta</span>,
							    <span itemprop="addressRegion">GA</span>
							    <span itemprop="postalCode">30312</span>
							  </div>
								<div itemprop="telephone">404.522.4484</div>
								<a href="http://www.blockloftsapartments.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/block-lofts/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/brookleigh-flats-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Brookleigh Flats</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">3450 Blair Circle NE</span><br>
							    <span itemprop="addressLocality">Atlanta</span>,
							    <span itemprop="addressRegion">GA</span>
							    <span itemprop="postalCode">30319</span>
							  </div>
								<div itemprop="telephone">770.451.8812</div>
								<a href="http://www.brookleighflats.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/brookleigh-flats/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/courtney-trace-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Courtney Trace</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1131 Courtney Trace Drive</span><br>
							    <span itemprop="addressLocality">Brandon</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">33511</span>
							  </div>
								<div itemprop="telephone">813.662.5566</div>
								<a href="http://www.courtneytrace.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/courtney-trace/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/cumberland-point-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Cumberland Pointe Apartments</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">15800 Navigation Way</span><br>
							    <span itemprop="addressLocality">Noblesville</span>,
							    <span itemprop="addressRegion">IN</span>
							    <span itemprop="postalCode">46060</span>
							  </div>
								<div itemprop="telephone">317.776.1400</div>
								<a href="http://www.cumberlandpointeapartments.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/cumberland-pointe-apartments/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/eastside-village-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Eastside Village</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1404 Vontess St., Sute 2119</span><br>
							    <span itemprop="addressLocality">Plano</span>,
							    <span itemprop="addressRegion">TX</span>
							    <span itemprop="postalCode">75074</span>
							  </div>
								<div itemprop="telephone">972.943.3050</div>
								<a href="http://www.eastsidevillageliving.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/eastside-village/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/glenwood-east-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Glenwood East</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">390 Stovall St SE</span><br>
							    <span itemprop="addressLocality">Atlanta</span>,
							    <span itemprop="addressRegion">GA</span>
							    <span itemprop="postalCode">30316</span>
							  </div>
								<div itemprop="telephone">404.627.4390</div>
								<a href="http://www.glenwoodeast.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/glenwood-east/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/grand-reserve-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Grand Reserve</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">3001 SW 24th Avenue</span><br>
							    <span itemprop="addressLocality">Ocala</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">34471</span>
							  </div>
								<div itemprop="telephone">352.854.6654</div>
								<a href="http://www.thegrandreserveocala.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/grand-reserve/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/hunters-creek-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Hunters Creek</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">100 Hunters Creek Blvd</span><br>
							    <span itemprop="addressLocality">Lebanon</span>,
							    <span itemprop="addressRegion">TN</span>
							    <span itemprop="postalCode">37087</span>
							  </div>
								<div itemprop="telephone">615.444.4324</div>
								<a href="http://www.livingathunterscreek.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/hunters-creek/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/lane-parke-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Lane Parke</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1000 Lane Parke Court</span><br>
							    <span itemprop="addressLocality">Mountain Brook</span>,
							    <span itemprop="addressRegion">AL</span>
							    <span itemprop="postalCode">35223</span>
							  </div>
								<div itemprop="telephone">205.443.4567</div>
								<a href="http://www.laneparkeapartments.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/lane-parke/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/legacy-meridian-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Legacy at Meridian</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1101 Exchange Place</span><br>
							    <span itemprop="addressLocality">Durham</span>,
							    <span itemprop="addressRegion">NC</span>
							    <span itemprop="postalCode">27713</span>
							  </div>
								<div itemprop="telephone">919.806.1988</div>
								<a href="http://www.legacyatmeridian.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/legacy-at-meridian/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/legacy-wakefield-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Legacy at Wakefield</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">14411 Calloway Gap Road</span><br>
							    <span itemprop="addressLocality">Raleigh</span>,
							    <span itemprop="addressRegion">NC</span>
							    <span itemprop="postalCode">27614</span>
							  </div>
								<div itemprop="telephone">919.554.9002</div>
								<a href="http://www.legacyatwakefield.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/legacy-at-wakefield/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/legacy-on-the-bay-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Legacy on the Bay</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">251 Vinings Way Blvd</span><br>
							    <span itemprop="addressLocality">Destin</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">32541</span>
							  </div>
								<div itemprop="telephone">850.650.7446</div>
								<a href="http://www.legacyonthebay.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/legacy-on-the-bay/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/losts-savannah-park-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Lofts at Savannah Park</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">5724 Byron Anthony Pl</span><br>
							    <span itemprop="addressLocality">Sanford</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">32771</span>
							  </div>
								<div itemprop="telephone">407.268.3303</div>
								<a href="http://loftssavannahpark.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/lofts-at-savannah-park/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/marsh-point-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Marsh Point</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">100A Marsh Point Drive</span><br>
							    <span itemprop="addressLocality">Hilton Head Island</span>,
							    <span itemprop="addressRegion">SC</span>
							    <span itemprop="postalCode">29926</span>
							  </div>
								<div itemprop="telephone">843.681.6550</div>
								<a href="http://www.marshpointapartments.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/marsh-point/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/preserve-spears-creek-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Preserve at Spears Creek</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">325 Spears Creek Church Rd</span><br>
							    <span itemprop="addressLocality">Elgin</span>,
							    <span itemprop="addressRegion">SC</span>
							    <span itemprop="postalCode">29045</span>
							  </div>
								<div itemprop="telephone">803.865.1100</div>
								<a href="http://www.preserveatspearscreek.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/preserve-at-spears-creek/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/preserve-steele-creek-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Preserve at Steele Creek</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">10830 Morgan Creek Drive</span><br>
							    <span itemprop="addressLocality">Charlotte</span>,
							    <span itemprop="addressRegion">NC</span>
							    <span itemprop="postalCode">28273</span>
							  </div>
								<div itemprop="telephone">704.588.2488</div>
								<a href="http://www.preserveatsteelecreek.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/preserve-at-steele-creek/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/riverhouse-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Riverhouse</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1200 Brookwood Drive</span><br>
							    <span itemprop="addressLocality">Little Rock</span>,
							    <span itemprop="addressRegion">AR</span>
							    <span itemprop="postalCode">72202</span>
							  </div>
								<div itemprop="telephone">501.725.8777</div>
								<a href="http://www.riverhouselittlerock.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/riverhouse/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/san-miguel-del-bosque-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">San Miguel Del Bosque</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">9180 Coors Blvd.</span><br>
							    <span itemprop="addressLocality">Albuquerque</span>,
							    <span itemprop="addressRegion">NM</span>
							    <span itemprop="postalCode">87120</span>
							  </div>
								<div itemprop="telephone">505.890.0009</div>
								<a href="http://www.sanmiguelapts.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/san-miguel-del-bosque/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/station-92-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Station 92</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">10247 GA-92</span><br>
							    <span itemprop="addressLocality">Woodstock</span>,
							    <span itemprop="addressRegion">GA</span>
							    <span itemprop="postalCode">30188</span>
							  </div>
								<div itemprop="telephone">678.500.9796</div>
								<a href="http://crestatlaurelwood.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/station-92/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/the-addison-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">The Addison</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">2516 Annapolis Way</span><br>
							    <span itemprop="addressLocality">Brandon</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">33511</span>
							  </div>
								<div itemprop="telephone">813.681.7200</div>
								<a href="http://www.theaddisonapartments.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/the-addison/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/addison-south-tryon-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">The Addison at South Tryon</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">7000 Modern Way</span><br>
							    <span itemprop="addressLocality">Charlotte</span>,
							    <span itemprop="addressRegion">NC</span>
							    <span itemprop="postalCode">28217</span>
							  </div>
								<div itemprop="telephone">980.237.6472</div>
								<a href="http://www.theaddisonatsouthtryon.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/the-addison-at-south-tryon/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/preserve-windsor-lake-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">The Preserve at Windsor Lake</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">1460 Oakcrest Dr</span><br>
							    <span itemprop="addressLocality">Columbia</span>,
							    <span itemprop="addressRegion">SC</span>
							    <span itemprop="postalCode">29223</span>
							  </div>
								<div itemprop="telephone">803.736.1099</div>
								<a href="http://www.PreserveAtWindsorLake.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/the-preserve-at-windsor-lake/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/q-maitland-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">The Q at Maitland</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">430 E Packwood Ave</span><br>
							    <span itemprop="addressLocality">Maitland</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">32751</span>
							  </div>
								<div itemprop="telephone">407.644.3663</div>
								<a href="http://www.theqatmaitland.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/the-q-at-maitland/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/retreat-panama-city-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">The Retreat at Panama City</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">90 Sandal Lane</span><br>
							    <span itemprop="addressLocality">Panama City</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">32413</span>
							  </div>
								<div itemprop="telephone">850.230.0074</div>
								<a href="http://www.retreatpcb.com" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/the-retreat-at-panama-city/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/tradition-summerville-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Tradition at Summerville</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">325 Marymeade Drive</span><br>
							    <span itemprop="addressLocality">Summerville</span>,
							    <span itemprop="addressRegion">SC</span>
							    <span itemprop="postalCode">29483</span>
							  </div>
								<div itemprop="telephone">843.821.4500</div>
								<a href="http://www.traditionapts.net/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/tradition-at-summerville/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/verandahs-brighton-bay-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Verandahs of Brighton Bay</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">10800 Brighton Bay Blvd NE</span><br>
							    <span itemprop="addressLocality">St. Petersburg</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">33716</span>
							  </div>
								<div itemprop="telephone">727.577.9800</div>
								<a href="http://www.theverandahsofbrightonbay.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/verandahs-of-brighton-bay/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/west-end-village-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">West End Village</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">221 31st Ave North</span><br>
							    <span itemprop="addressLocality">Nashville</span>,
							    <span itemprop="addressRegion">TN</span>
							    <span itemprop="postalCode">37203</span>
							  </div>
								<div itemprop="telephone">615.383.1322</div>
								<a href="http://www.westendvillageapartments.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/west-end-village/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
												
						<div class="communityGridItem" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<div class="column_attr">
								<div class="photo_wrapper">
									<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/03/whitepalm-lg-370x250.jpg" itemprop="photo">
								</div>
								<h5 class="themecolor" itemprop="name">Whitepalm</h5>
								<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							    <span itemprop="streetAddress">5400 S Williamson Blvd</span><br>
							    <span itemprop="addressLocality">Port Orange</span>,
							    <span itemprop="addressRegion">FL</span>
							    <span itemprop="postalCode">32128</span>
							  </div>
								<div itemprop="telephone">386.788.6658</div>
								<a href="http://www.whitepalmapts.com/" target="_blank">Visit Website</a>
								<div class="clearfix"></div>
								<a class="btn" href="http://23.235.205.152/~chadmin/location/whitepalm/"><span class="button_label">Details</span></a>
							</div>
						</div>
						
						
						
					</div>
				</div>
			</div>