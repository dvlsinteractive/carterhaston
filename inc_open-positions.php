<div class="openPositions">
	<h2>Open Positions</h2>
	
	<div class="positions">
		<div class="job">
			<p class="date">2/14/2017</p>
			<p class="title"><a href="#">Service Maintenance Director</a></p>
			<p class="location">Property: <a href="#">Legacy on the Bay - Destin, FL</a>
			<p class="type">Position Type: <strong>Apartment Community (Service Maintenance)</strong></p>
			<div class="clearfix"></div>
		</div><!--end job-->
		
		<div class="job">
			<p class="date">2/19/2017</p>
			<p class="title"><a href="#">Assistant Manager</a></p>
			<p class="location">Property: <a href="#">Marsh Point - Hilton Head Island, SC</a>
			<p class="type">Position Type: <strong>Apartment Community (Office)</strong></p>
			<div class="clearfix"></div>
		</div><!--end job-->
	</div>
	
	<div class="clearfix"></div>
	
	<div class="form">
		<p><strong>For information on this position or to submit your resume, please follow the link below.</strong></p>
		<form>
			<label>Name:</label>
			<input type="text">
			
			<label>Email Address:</label>
			<input type="text">
			
			<label>Cover Letter:</label>
			<textarea></textarea>
			
			<label>Upload Resume:</label>
			<input type="file" name="fileToUpload" id="fileToUpload">
			
			<button type="submit">Submit</button>
		</form>
	</div>
</div>