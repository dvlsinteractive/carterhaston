$(document).ready(function() {
	
	// new homepage carousel
	var carousel = $('.carousel');
	
	carousel.slick({
		 dots: true,
		 infinite: true,
		 speed: 400,
		 arrows: false,
		 appendDots: '.carousel-nav .dots',
		 swipe: true,
		 cssEase: 'ease-in-out'
	});
	
	$('.carousel-nav .prev').click(function(){
  	carousel.slick('slickPrev');
  	return false;
	});
	
	$('.carousel-nav .next').click(function(){
  	carousel.slick('slickNext');
  	return false;
	});
	

	// mobile menu
	$('.hamburger').click(function(){
		$('.hamburger').toggleClass('is-active');
		$('.offCanvas').toggleClass('active');
		$('html').toggleClass('locked');
		$('body').toggleClass('locked');
	});
	
	
	
	// sticky header/nav	
	var dir = 1;
	var MIN_TOP = 200;
	var MAX_TOP = 350;
	
	function sticky_relocate() {
	  var window_top = $(window).scrollTop();
	  var div_top = $('#sticky-anchor').offset().top;
	  if (window_top > div_top) {
	      $('#Top_bar').addClass('stick');
	      $('#sticky-anchor').height($('#Top_bar').outerHeight());
	  } else {
	      $('#Top_bar').removeClass('stick');
	      $('#sticky-anchor').height(0);
	  }
	}
	
	$(function() {
	  $(window).scroll(sticky_relocate);
	  sticky_relocate();
	});



	//change location to the community selected
	$('#communityChange').on('change', function(){
		if($(this).val()=="Community"){
			$("#SearchCity").prop('disabled',false);
			$("#SearchState").prop('disabled',false);
		}else{
			$("#SearchCity").prop('disabled',true);
			$("#SearchState").prop('disabled',true);		
		}
	});
	
	$('#communityFrm').submit(function(event){
		 
		 
		if($('#communityChange').val() !== "Community" ){
			event.preventDefault();
			window.location.href=$('#communityChange').val();
		}else{
			return true;
		}
	});
	
	
	$('#communityFrm button[type=reset]').on('click',function(){
		$("#SearchCity").prop('disabled',false);
		$("#SearchState").prop('disabled',false);
	})
	
	
	
	
	//filter cities based on selected state	
	$("#SearchCity").chained("#SearchState");
		
	
	// custom smooth scroll for timeline
	$('.yearLinks a[href*=#]').on('click', function(event){			
		var target = $(this.hash);
	  $('html,body').animate({scrollTop:target.offset().top - 120}, 500);
	  $('.yearLinks').find('ul').removeClass('open');
	  return false;
	});
	
	$('.yearLinks').find('p').click(function(){
		$(this).next('ul').toggleClass('open');
	});
	

}); //end document ready

// GOOGLE MAP CODE 
function initMap() { //callback triggered once the map script is pulled from google
	
	var myOptions = {
    zoom: 13,
    maxZoom:15,
    center: new google.maps.LatLng((35.9099586),-78.9121936),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"saturation":"0"},{"color":"#e1dfdf"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#d1d0d0"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#999999"},{"visibility":"on"}]}],
    draggable: true,
    zoomControl: true,
    mapTypeControl: false,
    streetViewControl: false,
    scrollwheel: false
};
	
	
	
	var map = new google.maps.Map(document.getElementById('map'), myOptions);
					
	var infowindow; //define infowindow as an IIFE
		(function() {
		    google.maps.Map.prototype.markers = new Array();
		    google.maps.Map.prototype.addMarker = function(marker) {
		      this.markers[this.markers.length] = marker;
		    };
		    google.maps.Map.prototype.getMarkers = function() {
		      return this.markers
		    };
		    google.maps.Map.prototype.clearMarkers = function() {
	        if (infowindow) {
	           infowindow.close();
	        }
	        for (var i = 0; i < this.markers.length; i++) {
	           this.markers[i].set_map(null);
	        }
		    };
		})();				
	
	var markers = [];//container for all markers to be used to set bounds of map
			
	function createMarker(name, latlng) {		
    var marker = new google.maps.Marker({ // create new marker with options
        position: latlng,
        icon:'http://23.235.205.152/~chadmin/site/web/themes/carterhaston/js/map-marker.png', // set icon for the marker
        map: map
    });
    
    markers.push(marker); // push the marker onto the markers array for use in creating boundaries
      
    google.maps.event.addListener(marker, "click", function() { // add click event to the markers in order to close other windows. 
        if (infowindow) infowindow.close();
        infowindow = new google.maps.InfoWindow({
            content: name
        });
        infowindow.open(map, marker);
    });
    return marker;
	} //end createMarker
		
  //loop json data
  Array.prototype.forEach.call(jsonpoints.locations, function(item) {
    var name = item.title || "";
    var address = item.properties.address || "";
    var type = item.properties.type || "";
    var website = item.properties.url || "";
    var phone = item.properties.phone || "";
    var city = item.properties.city || "";
    var state = item.properties.state || "";
    var point = new google.maps.LatLng(
        parseFloat(item.properties.lat),
        parseFloat(item.properties.lng)
        );
			
		var content = '<div class="iWindow"><div class="iwRow1 clearfix"><div class="iwTitle">'+name+'</div></div><div class="iwRow2 clearfix"><div class="iwAddressWrapper"><div class="iwAddress">'+address+'<br/>'+city+", "+state+'</div><div class="iwWebsite"><a href="'+website+'" target="_blank">Visit Website</a></div></div></div></div>';
		//add marker with info window content
		map.addMarker(createMarker(content, point));
		
		//set bounds of the map to encompass all the markers, based on the markers array populated in createMarker
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markers.length; i++) {
     bounds.extend(markers[i].getPosition());
    }
		map.fitBounds(bounds);
				
  });//end json loop			
  
  //mimic infowindow click if only one marker
	if(markers.length ==1){
		google.maps.event.trigger(markers[0], 'click');		
	}
	
  	
} //end map callback function 	
