// GOOGLE MAP CODE 
function initMap() { //callback triggered once the map script is pulled from google
	
	var myOptions = {
    zoom: 13,
    center: new google.maps.LatLng((35.9099586),-78.9121936),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"saturation":"0"},{"color":"#e1dfdf"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#d1d0d0"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#999999"},{"visibility":"on"}]}],
    draggable: true,
    zoomControl: true,
    mapTypeControl: false,
    streetViewControl: false,
    scrollwheel: false
};
	
	
	
	var map = new google.maps.Map(document.getElementById('map'), myOptions);
					
	var infowindow; //define infowindow as an IIFE
		(function() {
		    google.maps.Map.prototype.markers = new Array();
		    google.maps.Map.prototype.addMarker = function(marker) {
		      this.markers[this.markers.length] = marker;
		    };
		    google.maps.Map.prototype.getMarkers = function() {
		      return this.markers
		    };
		    google.maps.Map.prototype.clearMarkers = function() {
	        if (infowindow) {
	           infowindow.close();
	        }
	        for (var i = 0; i < this.markers.length; i++) {
	           this.markers[i].set_map(null);
	        }
		    };
		})();				
	
	var markers = [];//container for all markers to be used to set bounds of map
			
	function createMarker(name, latlng, type) {
		//custom marker
		var icon1 = { path: "M17.76,0a2.18,2.18,0,0,1,1.56.65A2.06,2.06,0,0,1,20,2.19V17.76A2.32,2.32,0,0,1,17.76,20H13.33L10,23.33,6.67,20H2.24a2.13,2.13,0,0,1-1.59-.68A2.17,2.17,0,0,1,0,17.76V2.19A2.08,2.08,0,0,1,.65.65,2.19,2.19,0,0,1,2.24,0Z", fillColor: '#000000', fillOpacity: .6, strokeWeight: 0, scale: 1};

    var marker = new google.maps.Marker({ // create new marker with options
        position: latlng,
        icon:'http://23.235.205.152/~chadmin/site/web/themes/carterhaston/js/map-marker.png', // set icon for the marker
        map: map,
        type:type
    });
    
    markers.push(marker); // push the marker onto the markers array for use in creating boundaries
      
    google.maps.event.addListener(marker, "click", function() { // add click event to the markers in order to close other windows. 
        if (infowindow) infowindow.close();
        infowindow = new google.maps.InfoWindow({
            content: name
        });
        infowindow.open(map, marker);
    });
    return marker;
	} //end createMarker
		
  //loop json data
  Array.prototype.forEach.call(jsonpoints.locations, function(item) {
    var name = item.title || "";
    var address = item.properties.address || "";
    var type = item.properties.type || "";
    var website = (typeof(item.properties.url) !== undefined && item.properties.url !="") ? '<div class="iwWebsite"><a href="'+item.properties.url+'" target="_blank">Visit Website</a></div>' : "";
    var phone = item.properties.phone || "";
    var city = (typeof(item.properties.city) !== undefined && item.properties.city !="") ? item.properties.city+", " : ""; //format city if present otherwise make empty string
    var state = (typeof(item.properties.state) !== undefined && item.properties.state !="" && city!="") ? item.properties.state : ""; //hide state if city not defined 
    var point = new google.maps.LatLng(
        parseFloat(item.properties.lat),
        parseFloat(item.properties.lng)
        );
		var content = '<div class="iWindow"><div class="iwRow1 clearfix"><div class="iwTitle">'+name+'</div></div><div class="iwRow2 clearfix"><div class="iwAddressWrapper"><div class="iwAddress">'+address+'<br/>'+city+state+'</div>'+website+'</div></div></div>';
		//add marker with info window content
		map.addMarker(createMarker(content, point, type));
		
		//set bounds of the map to encompass all the markers, based on the markers array populated in createMarker
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markers.length; i++) {
     bounds.extend(markers[i].getPosition());
    }
		map.fitBounds(bounds);
				
  });//end json loop			

console.log(markers);

  // when page has only 1 marker
		if(markers.length ==1){
			google.maps.event.trigger(markers[0], 'click'); //trigger click event
			setTimeout(function(){map.setZoom(18);	}, 200); // set zoom level a little higher than what fitBounds recommends. 
		}
  	
  	// open corporate marker on contact page 
	  for (var i = 0; i < markers.length; i++) {
		  if(markers[i].type=="corporate"){
			 google.maps.event.trigger(markers[i], 'click');
			 map.panTo(markers[i].getPosition())
		  }
	  }

} //end map callback function 	