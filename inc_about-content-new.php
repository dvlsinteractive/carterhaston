

<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				
				<div class="section mcb-section" style="padding-top:280px; padding-bottom:240px; background-color:" data-parallax="3d">
					<img class="mfn-parallax" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/staff.jpg" alt="parallax background" />
				</div>
				
				<div class="section mcb-section" style="padding-top:10px; padding-bottom:50px; ">
					<div class="section_wrapper mcb-section-inner">
						
						
						<div class="wrap mcb-wrap valign-top clearfix">
							<div class="mcb-wrap-inner aboutContent">
								
								
								<div class="aboutImg">
									<img src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/about-image.jpg" />
								</div>
								
								<div class="aboutText">
									<h2>About Carter-Haston</h2>
									<p>Carter-Haston was founded in 1987 and is celebrating its 25th anniversary as a knowledgeable and trustworthy leader in multifamily property management and private fund investment. Backed by 25 years of success, we have the investment and management foresight to maximize return on capital while managing risk through the acquisition, management and sale of multifamily communities throughout the United States.</p>
									<p>Carter-Haston’s portfolio of multifamily properties consists of nearly 10,000 units valued at more than $1 billion. Carter-Haston is based in Nashville and employs more than 300 people across the country with regional offices in Atlanta, Georgia; Tampa, Florida; Charleston, South Carolina and Dallas, Texas.</p>
<p>As a fully integrated real estate firm, Carter-Haston has significant experience in underwriting, acquisition, property management, disposition and equity and debt placement.</p>
								</div>
								
								<div class="clearfix"></div>
								
								<hr>
								
								<div class="splitCol">
									<h2 class="themecolor">Investment Foresight</h2>
									<div class="text">
									<p>At Carter-Haston, we distinguish ourselves by responding to the expectations and needs of you – our partners, clients and lenders.</p>
									<p>We’re attentive to your needs and provide timely, accurate and insightful information so you can make sound management and investment decisions.</p>
									<p>Carter-Haston’s success has been built upon our responsiveness to your expectations and our commitment to act on them with integrity. We are scrupulous in representing you and vigilant in our focus on results. We have consistently produced above-market returns over the last 25 years thanks to our intentional commitment and precise approach. Our investment foresight includes:</p>
									</div><!--end text-->
									
									<div class="iconGrid">
										<div class="iconText">
											<span class="icon2-search"></span>
											<p><strong>Unparalleled Knowledge</strong></p>
											<p>Providing a comprehensive, unbiased analysis of the marketplace, understanding the environment in which we operate and effectively responding and reporting as needed.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-stats"></span>
											<p><strong>Conscientious Reporting</strong></p>
											<p>Providing accurate financial results in a clear and timely manner.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-users"></span>
											<p><strong>Scrupulous Professionals</strong></p>
											<p>Building a team of knowledgeable, seasoned operating managers upon whom you can rely.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-alarmclock"></span>
											<p><strong>Trustworthy Recommendations</strong></p>
											<p>Making timely recommendations regarding the best utilization of assets.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-cogs"></span>
											<p><strong>Meaningful Management</strong></p>
											<p>Managing our properties to maximize the return on invested capital.</p>
										</div>
									</div><!--end iconGrid-->
									
									<hr>
									
									<div class="iconGrid">
										<h2 class="themecolor">Management Foresight</h2>
										<div class="iconText">
											<span class="icon2-user-tie"></span>
											<p><strong>Intentional Presentation</strong></p>
											<p>Providing a well-maintained and inviting place for residents and their guests. We recognize the importance of first impressions.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-handshake-o"></span>
											<p><strong>Dimensional Management</strong></p>
											<p>Committing to provide superior levels of service to residents from the first point of contact with residents through the last.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-home"></span>
											<p><strong>Exceptional Properties</strong></p>
											<p>Working diligently to ensure homes exceeds resident expectations upon move-in.</p>
										</div>
										<div class="clearfix"></div>
										
										<div class="iconText">
											<span class="icon2-room_service"></span>
											<p><strong>Attentive Service</strong></p>
											<p>Recognizing that occasional in-home maintenance is needed and working quickly and effectively to complete all requests while minimizing inconveniences. 24/7 emergency response is available across all of our communities.</p>
										</div>
										
										<div class="iconText">
											<span class="icon2-thumbs-up"></span>
											<p><strong>Customer Satisfaction</strong></p>
											<p>Measuring success by the attitude and feedback of our residents. The best way to make our communities better is to listen to those who have called them home.</p>
										</div>
									</div><!--end iconGrid-->
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>