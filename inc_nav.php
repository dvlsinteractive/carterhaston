<ul>
	<li<?php if($page == "home"){echo ' class="current-menu-item"';}?>><a href="index.php">Home</a></li>
	
	<li><a href="about.php">About Us</a>
		<ul class="sub-menu">
			<li class="menu-item-has-children"><a href="#_">Who We Are</a>
				<ul>
					<li><a href="executiveteam.php">Executive Team</a></li>
					<li><a href="propertymanagement.php">Property Management</a></li>
					<li><a href="#_">Capital Markets</a></li>
					<li><a href="#_">Accounting</a></li>
					<li><a href="supportstaff.php">Support Staff</a></li>
				</ul>
			</li>

			<li><a href="#_">History of Success</a></li>
		</ul>
	</li>
	
	<li><a href="#_">Investors</a></li>

	<li><a href="communities.php">Communities</a></li>
	
	<li><a href="thisjustin.php">This Just In</a></li>
	
	<li><a href="careers.php">Work With Us</a>
		<ul class="sub-menu">
			<li><a href="#_">Benefits</a></li>
			<li><a href="#_">Employment Philosophy</a></li>
			<li><a href="#_">Available Positions</a></li>
		</ul>
	</li>

	<li><a href="contact-us.php">Contact</a></li>
</ul>
