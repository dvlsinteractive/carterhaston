<?php
	$page = "executive-team";
	include('header.php');
?>


        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div class="section flv_sections_11">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Page Title-->
                                    <!-- One full width row-->
                                    <div class="column one column_fancy_heading">
                                        <div class="fancy_heading" style="padding-top:80px">
                                            
                                            <h2 class="title">Executive Team</h2>
                                            
                                        </div>
                                        <div class="column mcb-column one column_divider ">
                                            <hr class="" style="margin: 0 auto 70px;" />
                                        </div>
                                    </div>
                                    <!-- Page devider -->
                                    <!-- One full width row-->
                                    
                                    <!-- One full width row-->
                                    <div class="column one column_our_team_list">
                                        <!-- Team Member Area -->
                                        <div class="team team_list clearfix">
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="image_frame no_link scale-with-grid">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/Marc-Carter.jpg" alt="Marc Carter" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                            <div class="column one-second">
                                                <div class="desc_wrapper">
                                                    <h4>L. Marc Carter</h4>
                                                    <p class="subtitle">
                                                        CPM, Principal
                                                    </p>
                                                    <hr class="hr_color" />
                                                    <div class="desc">
                                                        <p>Marc is actively engaged in all aspects of the company with a particular focus on investment strategy, acquisitions and dispositions, and oversight of the property management platform.  Marc is co-chair of the Investment Committee with Harris.</p>

<p>Before founding Carter-Haston in 1987, Marc was president of Jacques-Miller Property Management from 1984 to 1987, a multi-faceted real estate investment firm with more than 22,000 units owned across 22 states and 56 cites.  Prior to Jacques-Miller, Marc headed management of the 10,000 unit portfolio of Towne Properties in Cincinnati, Ohio.  He began his career in real estate in 1972 with Multicon Properties in Columbus, Ohio.</p> 

<p>Marc is a Certified Property Manager, has sat on numerous civic and professional boards, is a director on the board of the National Multifamily Housing Council, and is a member of the National Apartment Association’s Executive Forum.  He attended Indiana University prior to graduating from Engineering Officer Candidate School.  He was honorably discharged in 1972.</p>
                                                     </div>
                                                </div>
                                            </div>
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="bq_wrapper">
	                                                <h4>Articles &amp; News</h4>
                                                    <p><ul>
	                                                 	<li><a href="#">Article Headline One</a></li>
	                                                 	<li><a href="#">Second Article Headline</a></li>
	                                                 	<li><a href="#">Third Article Headline</a></li>
                                                    </ul></p>
                                                    <div class="links">
                                                        <a href="mailto:" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page devider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_dots flv_margin_40">
                                            <span></span><span></span><span></span>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_our_team_list">
                                        <!-- Team Member Area -->
                                        <div class="team team_list clearfix">
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="image_frame no_link scale-with-grid">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/Harris-Haston.jpg" alt="Harris Haston" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                             <div class="column one-second">
                                                <div class="desc_wrapper">
                                                    <h4>C. Harris Haston</h4>
                                                    <p class="subtitle">
                                                        Principal
                                                    </p>
                                                    <hr class="hr_color" />
                                                    <div class="desc">
                                                        <p>Harris is actively engaged in all aspects of the company with a particular focus on investment strategy and oversight of corporate finance activities, debt and equity structures and investor relations.  Harris is co-chair of the Investment Committee with Marc. </p>

<p>Before founding Carter-Haston in 1987, Harris was president of Haston & Associates and Haston Real Estate, which provided development-consulting services to financial service firms and other institutional investors, and developed income-producing property for its own account.  He began his career in real estate in 1976 with First American National Bank, where his primary responsibility was real estate workouts from the 1974-1976 real estate recession. </p>

<p>Harris earned a B.S. with a major in Finance from The University of Tennessee, Knoxville.  He is currently a director on the board of the National Multifamily Housing Council, is a member of the Rotary Club of Nashville, where he was also treasurer, is an executive board member of the Middle Tennessee Council of Boy Scouts, and is a trustee board member of the Buffalo Bill Center of the West in Cody, Wyoming.</p>
                                                     </div>
                                                </div>
                                            </div>
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="bq_wrapper">
	                                                <h4>Articles &amp; News</h4>
                                                    <p><ul>
	                                                 	<li><a href="#">Article Headline One</a></li>
	                                                 	<li><a href="#">Second Article Headline</a></li>
	                                                 	<li><a href="#">Third Article Headline</a></li>
                                                    </ul></p>
                                                    <div class="links">
                                                        <a href="mailto:" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page devider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_dots flv_margin_40">
                                            <span></span><span></span><span></span>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_our_team_list">
                                        <!-- Team Member Area -->
                                        <div class="team team_list clearfix">
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="image_frame no_link scale-with-grid">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/Amanda-Speed.jpg" alt="Amanda Speed" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                            <div class="column one-second">
                                                <div class="desc_wrapper">
                                                    <h4>Amanda V. Speed</h4>
                                                    <p class="subtitle">
                                                        CPA, Partner
                                                    </p>
                                                    <hr class="hr_color" />
                                                    <div class="desc">
                                                        <p>Amanda joined the company in 1994 and is responsible for accounting, budgeting, treasury management,  investor reporting and ensuring compliance with tax and audit requirements.  She also oversees operational management functions, such as company policy and procedures, human resources, and information technology processes.  She is a member of the Investment Committee. </p>

<p>Amanda is a cum laude graduate of Vanderbilt University where she earned a B.A. in English.  She is a member of the National Multifamily Housing Council and the Tennessee Society of Certified Public Accountants, and is a Certified Public Accountant.</p>
                                                     </div>
                                                </div>
                                            </div>
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="bq_wrapper">
	                                                <h4>Articles &amp; News</h4>
                                                    <p><ul>
	                                                 	<li><a href="#">Article Headline One</a></li>
	                                                 	<li><a href="#">Second Article Headline</a></li>
	                                                 	<li><a href="#">Third Article Headline</a></li>
                                                    </ul></p>
                                                    <div class="links">
                                                        <a href="mailto:" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <!-- Page devider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_dots flv_margin_40">
                                            <span></span><span></span><span></span>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_our_team_list">
                                        <!-- Team Member Area -->
                                        <div class="team team_list clearfix">
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="image_frame no_link scale-with-grid">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/Michael-Fielder.jpg" alt="Michael Fielder" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                             <div class="column one-second">
                                                <div class="desc_wrapper">
                                                    <h4>Michael A. Fielder</h4>
                                                    <p class="subtitle">
                                                        Partner
                                                    </p>
                                                    <hr class="hr_color" />
                                                    <div class="desc">
                                                        <p>Michael joined the company in 1996 and is responsible for corporate finance activities, debt and equity structures, investor relations and reporting, and regulatory compliance.  Michael also has considerable experience with real estate screening and due diligence as well as managing the acquisition and disposition processes.  He is a member of the Investment Committee. </p>

<p>Michael earned a B.S. with a major in Finance from The University of Tennessee, Knoxville.  He is a member of the National Multifamily Housing Council and the Urban Land Institute, where he serves on the Nashville chapter’s capital markets advisory board, and is a licensed real estate broker in Tennessee.</p>
                                                     </div>
                                                </div>
                                            </div>
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="bq_wrapper">
	                                                <h4>Articles &amp; News</h4>
                                                    <p><ul>
	                                                 	<li><a href="#">Article Headline One</a></li>
	                                                 	<li><a href="#">Second Article Headline</a></li>
	                                                 	<li><a href="#">Third Article Headline</a></li>
                                                    </ul></p>
                                                    <div class="links">
                                                        <a href="mailto:" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Page devider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_dots flv_margin_40">
                                            <span></span><span></span><span></span>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_our_team_list">
                                        <!-- Team Member Area -->
                                        <div class="team team_list clearfix">
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="image_frame no_link scale-with-grid">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/John-Carter.jpg" alt="John Carter" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                             <div class="column one-second">
                                                <div class="desc_wrapper">
                                                    <h4>John T. Carter</h4>
                                                    <p class="subtitle">
                                                        Partner
                                                    </p>
                                                    <hr class="hr_color" />
                                                    <div class="desc">
                                                        <p>John joined the company in 1997 and is responsible for day-to-day oversight of the property management platform, with senior and regional property management teams reporting directly to him.  John also directs the acquisition and disposition processes and oversees property insurance, real estate tax matters and the construction management team.  He is a member of the Investment Committee.</p>

<p>John earned a B.A. in Public Affairs from Indiana University.  He is a member of the National Multifamily Housing Council and is a Certified Property Manager Candidate with the Institute of Real Estate Management. </p>
                                                     </div>
                                                </div>
                                            </div>
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="bq_wrapper">
	                                                <h4>Articles &amp; News</h4>
                                                    <p><ul>
	                                                 	<li><a href="#">Article Headline One</a></li>
	                                                 	<li><a href="#">Second Article Headline</a></li>
	                                                 	<li><a href="#">Third Article Headline</a></li>
                                                    </ul></p>
                                                    <div class="links">
                                                        <a href="mailto:" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page Divider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_dots flv_margin_40">
                                            <span></span><span></span><span></span>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_our_team_list">
                                        <!-- Team Member Area -->
                                        <div class="team team_list clearfix">
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="image_frame no_link scale-with-grid">
                                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/James-Shanks.jpg" alt="Douglas Wood" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                            <div class="column one-second">
                                                <div class="desc_wrapper">
                                                    <h4>James A. Shanks</h4>
                                                    <p class="subtitle">
                                                        Partner
                                                    </p>
                                                    <hr class="hr_color" />
                                                    <div class="desc">
                                                        <p>Jamie joined the company in 2007 and is responsible for real estate screening and due diligence as well as managing the acquisition and disposition processes.  Jamie is also involved in corporate finance activities, debt and equity structures, investor relations and reporting, and regulatory compliance.  He is a member of the Investment Committee. </p>

<p>Jamie earned a B.A. in Psychology from the University of Virginia and an M.B.A. from Belmont University.  He is a member of the National Multifamily Housing Council, the Urban Land Institute and the Board of Directors of the Historic Fourth Ward Park Conservancy in Atlanta, Georgia, and is a graduate of the Urban Land Institute’s Center for Leadership.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- One Fourth (1/4) Column -->
                                            <div class="column one-fourth">
                                                <div class="bq_wrapper">
                                                    <h4>Articles &amp; News</h4>
                                                    <p><ul>
	                                                 	<li><a href="#">Article Headline One</a></li>
	                                                 	<li><a href="#">Second Article Headline</a></li>
	                                                 	<li><a href="#">Third Article Headline</a></li>
                                                    </ul></p>

                                                    <div class="links">
                                                        <a href="mailto:" class="icon_bar icon_bar_small"><span class="t"><i class="icon-mail"></i></span><span class="b"><i class="icon-mail"></i></span></a><a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a><a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a><a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
      	
<?php include('footer.php'); ?>