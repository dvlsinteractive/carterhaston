

<!DOCTYPE html>

<html>

<head>

<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>Carter-Haston Multifamily</title>
<meta name="KEYWORDS" content="Carter-Haston (Corp. Office), apartments for rent, apartments in Nashville TN" />
<meta name="DESCRIPTION" content="Carter-Haston (Corp. Office) is Multfiamily Management and Ownership, based in Nashville, TN." />
<meta name="AUTHOR" content="Ellipse Communications, Inc." />
<meta name="COPYRIGHT" content="2017" />
<meta http-equiv="Content-Language" content="en-us" />
<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0" />



<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="/js/html5shiv.js"></script>
	<script src="/js/respond.min.js"></script>
<![endif]-->




	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRiWQAVq8ZO30aa-grooPn0bW5kc1GLWU"></script>
	
	<script type="text/javascript">
	
		var map;
		function initialize() {

						var latlng = new google.maps.LatLng((35.9099586),-78.9121936);
						var settings = {
							zoom: 8,
							center: latlng,
							scrollwheel: false,
							mapTypeControl: false,
							navigationControl: true,
							panControl: false,
							zoomControl: true,
							zoomControlOptions: {
								style: google.maps.ZoomControlStyle.LARGE,
								position: google.maps.ControlPosition.LEFT_CENTER
							},
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};

						map = new google.maps.Map(document.getElementById("mapWrapper"), settings);
						var bounds = new google.maps.LatLngBounds();
						

					// BEGIN Legacy at Meridian
					infowindow24627 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Legacy at Meridian</div><div class='iwPhone'>919.806.1988</div></div><div class='iwRow2 clearfix'><div class='iwAddressWrapper'><div class='iwAddress'>1101 Exchange Place<br/>Durham, NC 27713</div><div class='iwWebsite'><a href='http://www.legacyatmeridian.com/' target='_blank'>Visit Website</a></div></div></div></div>", maxWidth: 348});

					marker24627 = new google.maps.Marker({
						position: new google.maps.LatLng(35.9099586, -78.9121936),
						map: map,
						title: 'Legacy at Meridian',
						icon: '/images/Icon_Marker.png'
					});

					google.maps.event.addListener(marker24627, 'click', function() {
						closeWindows();
						map.panTo(marker24627.position);
						infowindow24627.open(map, marker24627);
					});

					bounds.extend(new google.maps.LatLng(Number(35.9099586),Number(-78.9121936)));


					// BEGIN Legacy at Wakefield
					infowindow24628 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Legacy at Wakefield</div><div class='iwPhone'>919.554.9002</div></div><div class='iwRow2 clearfix'><div class='iwAddressWrapper'><div class='iwAddress'>14411 Calloway Gap Road<br/>Raleigh, NC 27614</div><div class='iwWebsite'><a href='http://www.legacyatwakefield.com' target='_blank'>Visit Website</a></div></div></div></div>", maxWidth: 348});

					marker24628 = new google.maps.Marker({
						position: new google.maps.LatLng(35.9505427, -78.5437291),
						map: map,
						title: 'Legacy at Wakefield',
						icon: '/images/Icon_Marker.png'
					});

					google.maps.event.addListener(marker24628, 'click', function() {
						closeWindows();
						map.panTo(marker24628.position);
						infowindow24628.open(map, marker24628);
					});

					bounds.extend(new google.maps.LatLng(Number(35.9505427),Number(-78.5437291)));


					// BEGIN Preserve at Steele Creek
					infowindow14875 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Preserve at Steele Creek</div><div class='iwPhone'>704.588.2488</div></div><div class='iwRow2 clearfix'><div class='iwAddressWrapper'><div class='iwAddress'>10830 Morgan Creek Drive<br/>Charlotte, NC 28273</div><div class='iwWebsite'><a href='http://www.preserveatsteelecreek.com' target='_blank'>Visit Website</a></div></div></div></div>", maxWidth: 348});

					marker14875 = new google.maps.Marker({
						position: new google.maps.LatLng(35.1441006, -80.9745814),
						map: map,
						title: 'Preserve at Steele Creek',
						icon: '/images/Icon_Marker.png'
					});

					google.maps.event.addListener(marker14875, 'click', function() {
						closeWindows();
						map.panTo(marker14875.position);
						infowindow14875.open(map, marker14875);
					});

					bounds.extend(new google.maps.LatLng(Number(35.1441006),Number(-80.9745814)));


					// BEGIN The Addison at South Tryon
					infowindow24776 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>The Addison at South Tryon</div><div class='iwPhone'>980.237.6472</div></div><div class='iwRow2 clearfix'><div class='iwAddressWrapper'><div class='iwAddress'>7000 Modern Way<br/>Charlotte , NC 28217</div><div class='iwWebsite'><a href='http://www.theaddisonatsouthtryon.com' target='_blank'>Visit Website</a></div></div></div></div>", maxWidth: 348});

					marker24776 = new google.maps.Marker({
						position: new google.maps.LatLng(35.153822, -80.9225875),
						map: map,
						title: 'The Addison at South Tryon',
						icon: '/images/Icon_Marker.png'
					});

					google.maps.event.addListener(marker24776, 'click', function() {
						closeWindows();
						map.panTo(marker24776.position);
						infowindow24776.open(map, marker24776);
					});

					bounds.extend(new google.maps.LatLng(Number(35.153822),Number(-80.9225875)));

					map.fitBounds(bounds);


			// BEGIN Corp
			infowindowCorp = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Carter-Haston Corporate Headquarters</div></div></div>", maxWidth: 348});
			var myLatlng = new google.maps.LatLng(Number(36.1407343),Number(-86.8169478));
			var markerCorp = new google.maps.Marker({
				position: myLatlng,
				icon: '/images/spMapPin.png',
				map: map,
				title: 'Carter-Haston Corporate Headquarters'
			});
			google.maps.event.addListener(markerCorp, 'click', function() {
				closeWindows();
				map.panTo(markerCorp.position);
				infowindowCorp.open(map, markerCorp);
			});

			// BEGIN Region1
			infowindowRegion1 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Carter-Haston Regional Headquarters</div></div></div>", maxWidth: 348});
			var myLatlng1 = new google.maps.LatLng(33.787960, -84.385180);
			var markerRegion1 = new google.maps.Marker({
				position: myLatlng1,
				icon: '/images/spMapPin2.png',
				map: map,
				title: 'Carter-Haston Regional Headquarters'
			});
			google.maps.event.addListener(markerRegion1, 'click', function() {
				closeWindows();
				map.panTo(markerRegion1.position);
				infowindowRegion1.open(map, markerRegion1);
			});

			// BEGIN Region2
			infowindowRegion2 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Carter-Haston Regional Headquarters</div></div></div>", maxWidth: 348});
			var myLatlng2 = new google.maps.LatLng(32.7896656, -79.8834847);
			var markerRegion2 = new google.maps.Marker({
				position: myLatlng2,
				icon: '/images/spMapPin2.png',
				map: map,
				title: 'Carter-Haston Regional Headquarters'
			});
			google.maps.event.addListener(markerRegion2, 'click', function() {
				closeWindows();
				map.panTo(markerRegion2.position);
				infowindowRegion2.open(map, markerRegion2);
			});

			// BEGIN Region3
			infowindowRegion3 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Carter-Haston Regional Headquarters</div></div></div>", maxWidth: 348});
			var myLatlng3 = new google.maps.LatLng(28.0327593, -82.4516492);
			var markerRegion3 = new google.maps.Marker({
				position: myLatlng3,
				icon: '/images/spMapPin2.png',
				map: map,
				title: 'Carter-Haston Regional Headquarters'
			});
			google.maps.event.addListener(markerRegion3, 'click', function() {
				closeWindows();
				map.panTo(markerRegion3.position);
				infowindowRegion3.open(map, markerRegion3);
			});

			// BEGIN Region4
			infowindowRegion4 = new google.maps.InfoWindow({content: "<div class='iWindow'><div class='iwRow1 clearfix'><div class='iwTitle'>Carter-Haston Regional Headquarters</div></div></div>", maxWidth: 348});
			var myLatlng4 = new google.maps.LatLng(33.0566003,-96.6958319);
			var markerRegion4 = new google.maps.Marker({
				position: myLatlng4,
				icon: '/images/spMapPin2.png',
				map: map,
				title: 'Carter-Haston Regional Headquarters'
			});
			google.maps.event.addListener(markerRegion4, 'click', function() {
				closeWindows();
				map.panTo(markerRegion4.position);
				infowindowRegion4.open(map, markerRegion4);
			});
		} // END INITIALIZE

		function closeWindows() {
			infowindowCorp.close();
			infowindowRegion1.close();
			infowindowRegion2.close();
			infowindowRegion3.close();
			infowindowRegion4.close();
			
				infowindow24627.close();
			
				infowindow24628.close();
			
				infowindow14875.close();
			
				infowindow24776.close();
			
		}
	</script>
</head>

<body>

	<div id="mapWrapper" style="height: 500px; width: 100%;"></div>
	
	
	<div class="siteWidth">
		<div class="contentWrapper clearfix">
			<h1 class="pageTitle">Search Communities</h1>
			<div class="w25 floatLeft mobiBlock">
				<form id="SearchForm" name="SearchForm" method="post" action="communities.asp">
					<h3 class="contactTitle">Filter Your Results</h3>
					<div class="SearchInpSelectHolderState clearfix">
						<div class="SearchInpSelect">
							<select id="SearchProperty" name="SearchProperty">
								<option value="">Community</option>

									<option >Ansley Commons</option>

									<option >Arbors Harbor Town</option>

									<option >Barclay at Dunwoody</option>

									<option >Bay Oaks</option>

									<option >Block Lofts</option>

									<option >Brookleigh Flats</option>

									<option >Courtney Trace</option>

									<option >Cumberland Pointe Apartments</option>

									<option >Eastside Village</option>

									<option >Glenwood East</option>

									<option >Grand Reserve</option>

									<option >Hunters Creek</option>

									<option >Lane Parke</option>

									<option >Legacy at Meridian</option>

									<option >Legacy at Wakefield</option>

									<option >Legacy on the Bay</option>

									<option >Lofts at Savannah Park</option>

									<option >Marsh Point</option>

									<option >Preserve at Spears Creek</option>

									<option >Preserve at Steele Creek</option>

									<option >Riverhouse</option>

									<option >San Miguel Del Bosque</option>

									<option >Station 92</option>

									<option >The Addison</option>

									<option >The Addison at South Tryon</option>

									<option >The Preserve at Windsor Lake</option>

									<option >The Q at Maitland</option>

									<option >The Retreat at Panama City</option>

									<option >Tradition at Summerville</option>

									<option >Verandahs of Brighton Bay</option>

									<option >West End Village</option>

									<option >Whitepalm</option>

							</select>
						</div>
						<div class="SearchInpSelect">
							<select id="SearchState" name="SearchState">
								<option value="">State</option>

									<option >AL</option>

									<option >AR</option>

									<option >FL</option>

									<option >GA</option>

									<option >IN</option>

									<option selected>NC</option>

									<option >NM</option>

									<option >SC</option>

									<option >TN</option>

									<option >TX</option>

							</select>
						</div>
						<div class="SearchInpSelect">
							<select id="SearchCity" name="SearchCity">
								<option value="">City</option>

									<option class="AL" >Mountain Brook</option>

									<option class="AR" >Little Rock</option>

									<option class="FL" >Brandon</option>

									<option class="FL" >Destin</option>

									<option class="FL" >Maitland</option>

									<option class="FL" >Ocala</option>

									<option class="FL" >Panama City</option>

									<option class="FL" >Port Orange</option>

									<option class="FL" >Sanford</option>

									<option class="FL" >St. Petersburg</option>

									<option class="FL" >Tampa</option>

									<option class="GA" >Atlanta</option>

									<option class="GA" >Dunwoody</option>

									<option class="GA" >Woodstock</option>

									<option class="IN" >Noblesville</option>

									<option class="NC" >Charlotte</option>

									<option class="NC" >Durham</option>

									<option class="NC" >Raleigh</option>

									<option class="NM" >Albuquerque</option>

									<option class="SC" >Columbia</option>

									<option class="SC" >Elgin</option>

									<option class="SC" >Hilton Head Island</option>

									<option class="SC" >N. Charleston</option>

									<option class="SC" >Summerville</option>

									<option class="TN" >Lebanon</option>

									<option class="TN" >Memphis</option>

									<option class="TN" >Nashville</option>

									<option class="TX" >Plano</option>

							</select>
						</div>
					</div>
					<input type="submit" class="searchSubmit" value="Search" />
				</form><br>
			</div>
			<div class="floatRight w75 mobiBlock">
				<div class="clearfix">

								<div class="result mobiBlock clearfix">
									<div class="featuredPropsImg floatLeft "><a href="http://www.legacyatmeridian.com/" target="_blank"><img src="http://documents.ellipseinc.com/banners/LegacyAtMeridian.jpg" class="imgSizer"/></a></div>
									<div class="resultTitleHolder clearfix">
										<div class="resultTextTitle">
											<img alt="1" src="images/Icon_MarkerLG.png" class="ResultNumber" onClick="window.scrollTo(0, 0);closeWindows();map.setZoom(10);map.panTo(marker24627.position);infowindow24627.open(map, marker24627); "></a><span class="resultName">Legacy at Meridian</span>
										</div>
									</div>
									<div class="resultText">
										<p class="resultLoc">
											1101 Exchange Place<br>
											Durham,&nbsp;NC&nbsp;27713<br>
											919.806.1988
										</p>
										<a href="http://www.legacyatmeridian.com/" target="_blank" class="visitWebsiteBTN">Visit Website</a>
									</div>
								</div>

								<div class="result mobiBlock clearfix">
									<div class="featuredPropsImg floatLeft "><a href="http://www.legacyatwakefield.com" target="_blank"><img src="http://documents.ellipseinc.com/banners/LegacyAtWakefield.jpg" class="imgSizer"/></a></div>
									<div class="resultTitleHolder clearfix">
										<div class="resultTextTitle">
											<img alt="2" src="images/Icon_MarkerLG.png" class="ResultNumber" onClick="window.scrollTo(0, 0);closeWindows();map.setZoom(10);map.panTo(marker24628.position);infowindow24628.open(map, marker24628); "></a><span class="resultName">Legacy at Wakefield</span>
										</div>
									</div>
									<div class="resultText">
										<p class="resultLoc">
											14411 Calloway Gap Road<br>
											Raleigh,&nbsp;NC&nbsp;27614<br>
											919.554.9002
										</p>
										<a href="http://www.legacyatwakefield.com" target="_blank" class="visitWebsiteBTN">Visit Website</a>
									</div>
								</div>

								<div class="result mobiBlock clearfix">
									<div class="featuredPropsImg floatLeft "><a href="http://www.preserveatsteelecreek.com" target="_blank"><img src="http://documents.ellipseinc.com/banners/SteelCreekXS.jpg" class="imgSizer"/></a></div>
									<div class="resultTitleHolder clearfix">
										<div class="resultTextTitle">
											<img alt="3" src="images/Icon_MarkerLG.png" class="ResultNumber" onClick="window.scrollTo(0, 0);closeWindows();map.setZoom(10);map.panTo(marker14875.position);infowindow14875.open(map, marker14875); "></a><span class="resultName">Preserve at Steele Creek</span>
										</div>
									</div>
									<div class="resultText">
										<p class="resultLoc">
											10830 Morgan Creek Drive<br>
											Charlotte,&nbsp;NC&nbsp;28273<br>
											704.588.2488
										</p>
										<a href="http://www.preserveatsteelecreek.com" target="_blank" class="visitWebsiteBTN">Visit Website</a>
									</div>
								</div>

								<div class="result mobiBlock clearfix">
									<div class="featuredPropsImg floatLeft "><a href="http://www.theaddisonatsouthtryon.com" target="_blank"><img src="http://documents.ellipseinc.com/banners/Addisonpoolday3(1).jpg" class="imgSizer"/></a></div>
									<div class="resultTitleHolder clearfix">
										<div class="resultTextTitle">
											<img alt="4" src="images/Icon_MarkerLG.png" class="ResultNumber" onClick="window.scrollTo(0, 0);closeWindows();map.setZoom(10);map.panTo(marker24776.position);infowindow24776.open(map, marker24776); "></a><span class="resultName">The Addison at South Tryon</span>
										</div>
									</div>
									<div class="resultText">
										<p class="resultLoc">
											7000 Modern Way<br>
											Charlotte ,&nbsp;NC&nbsp;28217<br>
											980.237.6472
										</p>
										<a href="http://www.theaddisonatsouthtryon.com" target="_blank" class="visitWebsiteBTN">Visit Website</a>
									</div>
								</div>

				</div>
				<div class="clearfix">
					<div class="PageNavigation floatLeft">
						Showing 1 - 4 of 4 Communities
					</div>
					<div id="PageNavigationLinksContainer" class="PageNavigation floatRight clearfix">
						<div class="floatRight">

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>

</html>