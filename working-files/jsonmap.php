<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Using MySQL and PHP with Google Maps</title>
    <style>
      #map {
        height: 100%;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      .iWindow{
        min-width: 180px;
        text-align: center;
      }
      .iwTitle{
        color: #83AD52;
        font-family: 'bebasregular';
      }
      .iwWebsite {
        color: #ffffff;
        background: #83AD52;
        padding: 2px 0;
        margin: 5px auto;
        text-transform: uppercase;
        font-size: 12px;
      }

    </style>
  </head>

  <body>
    <div id="map"></div>

    <script>
      

//svg data for icons, 22px x 22px
var icon1 = {
    path: "M17.76,0a2.18,2.18,0,0,1,1.56.65A2.06,2.06,0,0,1,20,2.19V17.76A2.32,2.32,0,0,1,17.76,20H13.33L10,23.33,6.67,20H2.24a2.13,2.13,0,0,1-1.59-.68A2.17,2.17,0,0,1,0,17.76V2.19A2.08,2.08,0,0,1,.65.65,2.19,2.19,0,0,1,2.24,0Z",
    fillColor: '#000000',
    fillOpacity: .6,
    strokeWeight: 0,
    scale: 1
}

var icon2 = {
    path: "M6.6,8.95A2.34,2.34,0,0,0,8.95,6.6,2.35,2.35,0,0,0,6.6,4.25,2.34,2.34,0,0,0,4.25,6.6,2.34,2.34,0,0,0,6.6,8.95ZM6.6,0a6.37,6.37,0,0,1,4.67,1.93A6.38,6.38,0,0,1,13.2,6.6a8.92,8.92,0,0,1-.69,3.15,19.18,19.18,0,0,1-1.66,3.32q-1,1.55-1.93,2.9c-.63.9-1.18,1.62-1.62,2.15l-.71.75-.71-.82c-.3-.34-.83-1-1.59-2a33.46,33.46,0,0,1-2-3A21.82,21.82,0,0,1,.71,9.79,9,9,0,0,1,0,6.6,6.37,6.37,0,0,1,1.93,1.93,6.38,6.38,0,0,1,6.6,0Z",
    fillColor: '#FF0000',
    fillOpacity: .6,
    strokeWeight: 0,
    scale: 1
}

var icon3 = {
    path: "M7.68,0a7.4,7.4,0,0,1,5.43,2.24,7.41,7.41,0,0,1,2.24,5.44,10.37,10.37,0,0,1-.8,3.66,22.31,22.31,0,0,1-1.93,3.86q-1.13,1.8-2.24,3.37c-.74,1-1.37,1.88-1.88,2.5l-.82.88-.82-1c-.34-.4-1-1.18-1.85-2.37a38.92,38.92,0,0,1-2.34-3.45A25.38,25.38,0,0,1,.82,11.39,10.42,10.42,0,0,1,0,7.68,7.4,7.4,0,0,1,2.24,2.24,7.41,7.41,0,0,1,7.68,0Z",
    fillColor: '#3DC7F4',
    fillOpacity: .8,
    strokeWeight: 0,
    scale: 1
}
  //map type from location data to icon svg data
  var customIcon = {
        community:{
          svg:icon1
        },
        regional:{
          svg:icon2
        },
        corporate:{
          svg:icon3
        }
      };


  var jsonpoints = {
    "locations": [
      { 
        "title":"Arbors Harbor Town",
        "properties":{
          "lat": 35.1600782,
          "lng": -90.0552072,
          "address":"671 Harbor Edge Drive",
          "city":"Memphis",
          "state":"TN",
          "zip":"38103",
          "phone":"901.526.0322",
          "type":"community",
          "url":"http://www.arborsharbortown.com/"
        }
      },
      { 
        "title":"Barclay at Dunwoody",
        "properties":{
          "lat": 33.9296433,
          "lng": -84.2989922,
          "address":"4580 Barclay Drive",
          "city":"Dunwoody",
          "state":"GA",
          "zip":"30338",
          "phone":"",
          "type":"community",
          "url":"http://www.barclaydunwoody.com/"
        }
      }
    ]
  };

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng((35.9099586),-78.9121936),
          zoom: 7
          });

  var markers = [];

  //loop json data
  Array.prototype.forEach.call(jsonpoints.locations, function(item) {
    //console.log(item.title+":"+item.properties.city);

    var name = item.title || "";
    var address = item.properties.address || "";
    var type = item.properties.type || "";
    var website = item.properties.url || "";
    var phone = item.properties.phone || "";
    var city = item.properties.city || "";
    var state = item.properties.state || "";
    var point = new google.maps.LatLng(
        parseFloat(item.properties.lat),
        parseFloat(item.properties.lng)
        );

    var icondata = customIcon[type] || {};
    var marker = new google.maps.Marker({
      map: map,
      position: point,
      icon: icondata.svg
    });
    markers.push(marker);

    marker.info = new google.maps.InfoWindow({content: '<div class="iWindow"><div class="iwRow1 clearfix"><div class="iwTitle">'+name+'</div><div class="iwPhone">'+phone+'</div></div><div class="iwRow2 clearfix"><div class="iwAddressWrapper"><div class="iwAddress">'+address+'<br/>'+city+", "+state+'</div><div class="iwWebsite"><a href="'+website+'" target="_blank">Visit Website</a></div></div></div></div>', maxWidth: 348});
    google.maps.event.addListener(marker, 'click', function() { marker.info.open(map, marker); });


    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markers.length; i++) {
     bounds.extend(markers[i].getPosition());
    }
    map.fitBounds(bounds);
   
  });//end json loop


};//end initMap


    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRiWQAVq8ZO30aa-grooPn0bW5kc1GLWU&callback=initMap">
    </script>
  </body>
</html>