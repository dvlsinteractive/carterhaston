

<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				
				<div class="section mcb-section" style="padding-top:280px; padding-bottom:240px; background-color:" data-parallax="3d">
					<img class="mfn-parallax" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/staff.jpg" alt="parallax background" />
				</div>
				
				<div class="section mcb-section" style="padding-top:80px; padding-bottom:50px; ">
					<div class="section_wrapper mcb-section-inner">
						
						
						<div class="wrap mcb-wrap one-third  valign-top clearfix">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item no_link scale-with-grid no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/about-image.jpg" alt="home_architect2_studio1" width="344" height="326" />
										</div>
									</div>
								</div>
							</div>
						</div>
					
					
						<div class="wrap mcb-wrap two-third  valign-top clearfix" style="padding:0 0 0 5%">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_column  column-margin-20px">
									<div class="column_attr"><h2>About Carter-Haston</h2></div>
								</div>
								
								<div class="column mcb-column column_column">
									<div class="column_attr">
										<p>Carter-Haston was founded in 1987 and is celebrating its 25th anniversary as a knowledgeable and trustworthy leader in multifamily property management and private fund investment. Backed by 25 years of success, we have the investment and management foresight to maximize return on capital while managing risk through the acquisition, management and sale of multifamily communities throughout the United States.</p>
<p>Carter-Haston’s portfolio of multifamily properties consists of nearly 10,000 units valued at more than $1 billion. Carter-Haston is based in Nashville and employs more than 300 people across the country with regional offices in Atlanta, Georgia; Tampa, Florida; Charleston, South Carolina and Dallas, Texas.</p>
<p>As a fully integrated real estate firm, Carter-Haston has significant experience in underwriting, acquisition, property management, disposition and equity and debt placement.</p>
									</div>
								</div>								
							</div>
						</div>
					
					
					
					
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_divider">
									<hr class="no_line" style="margin: 0 auto 30px;" />
								</div>
								
								<div class="column mcb-column one column_divider">
									<hr class="" style="margin: 0 auto 70px;" />
								</div>
								
																	
									
																		<div class="column mcb-column one column_column">
										<div class="column_attr align_center" style=" padding:0 7%;">
											<h2 class="themecolor">Investment Foresight</h2>
										</div>
									</div>
									
									<div class="column mcb-column one-second column_column">
										<div class="column_attr">
											<p>At Carter-Haston, we distinguish ourselves by responding to the expectations and needs of you – our partners, clients and lenders.</p>
<p>We’re attentive to your needs and provide timely, accurate and insightful information so you can make sound management and investment decisions.</p>
										</div>
									</div>
		
									<div class="column mcb-column one-second column_column">
										<div class="column_attr">
											<p>Carter-Haston’s success has been built upon our responsiveness to your expectations and our commitment to act on them with integrity. We are scrupulous in representing you and vigilant in our focus on results. We have consistently produced above-market returns over the last 25 years thanks to our intentional commitment and precise approach. Our investment foresight includes:</p>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
	
							<div class="section aboutIcons" id="lists flv_sections_16">
								<div class="section_wrapper clearfix">
									<div class="items_group clearfix">
		
										<div class="column one column_divider">
											<hr class="no_line flv_margin_40" />
										</div>
										
										
																					<!-- One Third (1/3) Column -->
											<div class="column one-third column_list">
												<div class="list_item lists_2 clearfix">
														<div class="list_left list_image icon">
															<span class="icon2-search"></span>
														</div>
														<div class="list_right">
															<h4>Unparalleled Knowledge</h4>
															<div class="desc">Providing a comprehensive, unbiased analysis of the marketplace, understanding the environment in which we operate and effectively responding and reporting as needed.</div>
														</div>
												</div>
											</div>
																					<!-- One Third (1/3) Column -->
											<div class="column one-third column_list">
												<div class="list_item lists_2 clearfix">
														<div class="list_left list_image icon">
															<span class="icon2-stats"></span>
														</div>
														<div class="list_right">
															<h4>Conscientious Reporting</h4>
															<div class="desc">Providing accurate financial results in a clear and timely manner.</div>
														</div>
												</div>
											</div>
																					<!-- One Third (1/3) Column -->
											<div class="column one-third column_list">
												<div class="list_item lists_2 clearfix">
														<div class="list_left list_image icon">
															<span class="icon2-users"></span>
														</div>
														<div class="list_right">
															<h4>Scrupulous Professionals</h4>
															<div class="desc">Building a team of knowledgeable, seasoned operating managers upon whom you can rely.</div>
														</div>
												</div>
											</div>
																					<!-- One Third (1/3) Column -->
											<div class="column one-third column_list">
												<div class="list_item lists_2 clearfix">
														<div class="list_left list_image icon">
															<span class="icon2-alarmclock"></span>
														</div>
														<div class="list_right">
															<h4>Trustworthy Recommendations</h4>
															<div class="desc">Making timely recommendations regarding the best utilization of assets.</div>
														</div>
												</div>
											</div>
																					<!-- One Third (1/3) Column -->
											<div class="column one-third column_list">
												<div class="list_item lists_2 clearfix">
														<div class="list_left list_image icon">
															<span class="icon2-cogs"></span>
														</div>
														<div class="list_right">
															<h4>Meaningful Management</h4>
															<div class="desc">Managing our properties to maximize the return on invested capital.</div>
														</div>
												</div>
											</div>
																			
												          
										<div class="column one column_divider">
											<hr class="flv_margin_40" />
										</div>
										
									
									
									
									
									
									
																	
								

																	
									
																		<div class="column mcb-column one column_column">
										<div class="column_attr align_center" style=" padding:0 7%; margin-top:80px;">
											<h2 class="themecolor">Management Foresight</h2>
										</div>
									</div>
	
									<div class="column mcb-column one-second column_column">
										<div class="column_attr">
																					</div>
									</div>
						
									<div class="column mcb-column one-second column_column">
										<div class="column_attr">
																					</div>
									</div>
								</div><!--end items_group-->
					
					
																<!-- One Fourth (1/4) Column -->
								<div class="column one-fourth column_list">
									<div class="list_item lists_3 clearfix">
										<div class="list_left list_image icon">
											<span class="icon2-user-tie"></span>
										</div>
										
										<div class="list_right">
											<h4>Intentional Presentation</h4>
											<div class="desc">Providing a well-maintained and inviting place for residents and their guests. We recognize the importance of first impressions.</div>
										</div>
									</div>
								</div>
																<!-- One Fourth (1/4) Column -->
								<div class="column one-fourth column_list">
									<div class="list_item lists_3 clearfix">
										<div class="list_left list_image icon">
											<span class="icon2-paste"></span>
										</div>
										
										<div class="list_right">
											<h4>Dimensional Management</h4>
											<div class="desc">Committing to provide superior levels of service to residents from the first point of contact with residents through the last.</div>
										</div>
									</div>
								</div>
																<!-- One Fourth (1/4) Column -->
								<div class="column one-fourth column_list">
									<div class="list_item lists_3 clearfix">
										<div class="list_left list_image icon">
											<span class="icon2-home"></span>
										</div>
										
										<div class="list_right">
											<h4>Exceptional Properties</h4>
											<div class="desc">Working diligently to ensure homes exceeds resident expectations upon move-in.</div>
										</div>
									</div>
								</div>
																<!-- One Fourth (1/4) Column -->
								<div class="column one-fourth column_list">
									<div class="list_item lists_3 clearfix">
										<div class="list_left list_image icon">
											<span class="icon2-room_service"></span>
										</div>
										
										<div class="list_right">
											<h4>Attentive Service</h4>
											<div class="desc">Recognizing that occasional in-home maintenance is needed and working quickly and effectively to complete all requests while minimizing inconveniences. 24/7 emergency response is available across all of our communities.
</div>
										</div>
									</div>
								</div>
																<!-- One Fourth (1/4) Column -->
								<div class="column one-fourth column_list">
									<div class="list_item lists_3 clearfix">
										<div class="list_left list_image icon">
											<span class="icon2-thumbs-up"></span>
										</div>
										
										<div class="list_right">
											<h4>Customer Satisfaction</h4>
											<div class="desc">Measuring success by the attitude and feedback of our residents. The best way to make our communities better is to listen to those who have called them home.</div>
										</div>
									</div>
								</div>
													
												
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>