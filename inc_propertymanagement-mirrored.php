<div class="entry-content">
				<div class="section flv_sections_11">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
												
						<div class="column one column_fancy_heading">
							<div class="fancy_heading" style="padding-top:80px">
								<h2 class="title">Property Managment</h2>
							</div>
							
							<div class="column mcb-column one column_divider ">
								<hr class="" style="margin: 0 auto 70px;">
							</div>
						</div>
						

												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/john-t-carter.jpg" alt="John T. Carter">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>John T. Carter</h4>
										<p class="subtitle">Partner</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/tom-baird.jpg" alt="Tom Baird">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Tom Baird</h4>
										<p class="subtitle">Senior Vice President</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/chris-carter.jpg" alt="Chris Carter">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Chris Carter</h4>
										<p class="subtitle">PMIC, CAM, Regional Vice President</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/nicole-storey.jpg" alt="Nicole Storey">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Nicole Storey</h4>
										<p class="subtitle">CAM, Regional Vice President</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/john-seider.jpg" alt="John Seider">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>John Seider</h4>
										<p class="subtitle">CAM, Regional Manager</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/miyon-moore.jpg" alt="Miyon Moore">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Miyon Moore</h4>
										<p class="subtitle">Marketing and Training Director</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/allyson-andrews.jpg" alt="Allyson Andrews">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Allyson Andrews</h4>
										<p class="subtitle">Information Systems Director</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/natalie-hall.jpg" alt="Natalie Hall">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Natalie Hall</h4>
										<p class="subtitle">Client Services Director</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/wesley-jones.jpg" alt="Wesley Jones">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Wesley Jones</h4>
										<p class="subtitle">CAM, Revenue Manager</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/mike-chiariello.jpg" alt="Mike Chiariello">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Mike Chiariello</h4>
										<p class="subtitle">Director of Capital Projects</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/ashley-allen.jpg" alt="Ashley Allen">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Ashley Allen</h4>
										<p class="subtitle">Marketing and Training Director</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/melinda-wilson.jpg" alt="Melinda Wilson">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Melinda Wilson</h4>
										<p class="subtitle">ARM, Area Manager</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/michelle-sweeney.jpg" alt="Michelle Sweeney">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Michelle Sweeney</h4>
										<p class="subtitle">CAM, Training and Development Director</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
												
													
						<div class="team4column">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="http://23.235.205.152/~chadmin/site/web/assets/2017/02/jason-west.jpg" alt="Jason West">
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr desc">
										<h4>Jason West</h4>
										<p class="subtitle">Regional Facilities Director</p>
                    <hr class="no_line" style="margin: 0 auto 20px;">
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="http://23.235.205.152/~chadmin/site/web/themes/carterhaston/images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3">
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;">
								</div>
							</div>
						</div>
						</div>
							
							
												
																				
						</div>
					</div>
				</div>
			</div>