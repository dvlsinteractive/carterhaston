<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Carter-Haston</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel='stylesheet' id='Raleway-css' href='http://fonts.googleapis.com/css?family=Raleway:400,400italic,700,700italic,900'>
<link rel='stylesheet' id='global-css' href='css/combined.min.css'>
<link rel='stylesheet' href="css/fonts/icons/icons.css">
<link rel='stylesheet' id='global-css' href='css/custom.css'>
<link rel='stylesheet' id='global-css' href='css/customSelect.css'>

</head>

<body class="<?php if($carousel == true) { echo "template-slider "; } ?>color-custom style-simple layout-full-width mobile-tb-left button-flat if-zoom no-content-padding header-below header-fw minimalist-header-no sticky-header sticky-white ab-hide subheader-both-center menu-line-below menuo-no-borders menuo-right footer-copy-center" id="debug">	

<div id="Wrapper">
	
	<?php if($page == "home") { include('inc_headerCarousel.php'); } ?>
	
	<div id="Header_wrapper" style="background:url('images/home_architect2_footer_bg-1.jpg');">
		<header id="Header">
			
			<div class="offCanvas">
				<button class="hamburger hamburger--elastic" type="button" id="closeMenu">
				  <span class="hamburger-box">
				    <span class="hamburger-inner"></span>
				  </span>
				  <span class="label">
				  	<span class="close">Close</span>
				  </span>
				</button>
				
				<div class="menuWrapper">
					<nav class="mobileMenu"><?php include('inc_nav.php'); ?></nav>
				</div>
			</div>
			
			<div id="sticky-anchor"></div>
			<div id="Top_bar">
				<div class="logoNew">
					<a href="index.php" title="Carter-Haston">
						<span class="align"></span>
						<img src="images/carterhlogo.png" alt="CarterHaston Logo" />
					</a>
				</div>
				
				<nav class="mainMenu"><?php include('inc_nav.php'); ?></nav>
				
				<button class=" hamburger hamburger--elastic" type="button" id="menuLink">
				  <span class="hamburger-box">
				    <span class="hamburger-inner"></span>
				  </span>
				  <span class="label">
				  	<span class="open">Menu</span>
				  	<span class="close">Close</span>
				  </span>
				</button>
			</div><!--end #Top_bar-->
			
			
		</header>
	</div><!--end Header_wrapper-->