<?php
	$page = "communities";
	$map = true;
	include('header.php'); 
?>

<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<div class="section mcb-section full-width no-margin-h no-margin-v  " style="padding-top:10px; padding-bottom:20px; ">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_map ">

									<div class="google-map-wrapper no_border">
										<div  id="map" style="width:100%; height:550px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--end sections_group-->

		<div class="entry-content">
			<?php include('inc_communities-new.php'); ?>
		</div><!--end entry-content-->
	</div><!--end content_wrapper-->
</div><!--end Content-->


<?php include('footer.php'); ?>