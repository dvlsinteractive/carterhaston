// load Gulp dependencies
var gulp = require('gulp'); 
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var combineMq = require('gulp-combine-mq');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');
var cssbeautify = require('gulp-cssbeautify');



function onError(err) {
    console.log(err);
}


// watch SCSS
gulp.task('styles', function () {

  // compile scss
	return sass('sass/main.scss')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    
     // automatically add needed browser prefixing
    .pipe(autoprefixer({
        browsers: ['> 5%', 'IE 9','last 2 versions'],
        cascade: false,
        remove: true
    }))
    
    // combine media queries & move to end of document
    .pipe(combineMq({
        beautify: false
    }))
    
    // format CSS
    .pipe(cssbeautify({
	    indent: '  ',
	    openbrace: 'end-of-line',
	    autosemicolon: true
    }))
    
	  // rename in folder
		.pipe(rename('custom.css'))			
    .pipe(gulp.dest('../css'))
    
    // re-inject styles into page
    .pipe(livereload());
});




// watch files for changes
gulp.task('watch', function() {	
	livereload.listen();

  //watch and reload PHP and html
  gulp.watch('../**/*.php').on('change', function(file) {
  	livereload.changed(file.path); 
  });
  
  gulp.watch('../**/*.html').on('change', function(file) {
  	livereload.changed(file.path);
 	});

  gulp.watch('sass/*.scss', ['styles']);
  gulp.watch('sass/**/*.scss', ['styles']);
});



gulp.task('default', ['styles', 'watch']);


// manual tasks

// combine all BeTheme files (manually run gulp tasks to compile)
gulp.task('combineCSS', function() {
  return gulp.src('BeTheme_source/css/*.css')
    .pipe(concat('combined.min.css'))
    .pipe(combineMq({
        beautify: false
    }))
    .pipe(cleanCSS({
	    keepBreaks: true,
	    processImport: true
	  }))
    .pipe(gulp.dest('../css/'));
});

gulp.task('combineJS', function() {
  return gulp.src('BeTheme_source/js/*.js')
    .pipe(concat('combined.js'))
    .pipe(uglify())
    .pipe(gulp.dest('../js/'));
});

// create minified CSS
gulp.task('minify', function() {
  return gulp.src('../css/custom.css')
    .pipe(cleanCSS({
	    keepBreaks: true,
	    processImport: true
	  }))
	  .pipe(rename('custom.min.css'))
    .pipe(gulp.dest('../css/'));
});
