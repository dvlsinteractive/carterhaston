<?php
	$page = "single-location";
	include('header.php');
?>


<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<div class="section mcb-section full-width no-margin-h no-margin-v  " style="padding-top:10px; padding-bottom:20px; ">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner singleJobPost">
								
								<div class="description">
									<h1>Job Title</h1>
									<div class="jobInfo">
										<p>Posted: <strong>3/2/17</strong></p>
										<p>Location: <strong>Job location here</strong> <a href="#">(website)</a></p>
									</div>
									
									<div class="clearfix"></div>
								
									<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>

									<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>

									<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere</p>
								</div>
								
								<div class="applyForm">
									<h2>Apply Now</h2>
									<noscript class="ninja-forms-noscript-message">
    Notice: JavaScript is required for this content.</noscript><div id="nf-form-2-cont" class="nf-form-cont">
	<span class="nf-form-title">
		<h3></h3>
	</span>
	<div class="nf-form-wrap ninja-forms-form-wrap">
		<div class="nf-response-msg"></div>
		<div class="nf-debug-msg"></div>
		<div class="nf-before-form"><nf-section>
	
</nf-section></div>
		<div class="nf-form-layout"><div>
		<div class="nf-before-form-content"><nf-section>
    <div class="nf-form-fields-required">Fields marked with an <span class="ninja-forms-req-symbol">*</span> are required</div>
    
</nf-section></div>
		<div class="nf-form-content "><nf-fields-wrap><nf-field>
    <div id="nf-field-8-container" class="nf-field-container firstname-container  label-above half ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-8-wrap" class="field-wrap firstname-wrap" data-field-id="8">
		
		
		
	<div class="nf-field-label"><label for="nf-field-8" class="">First Name  </label></div>

		
		<div class="nf-field-element">
    <input id="nf-field-8" name="nf-field-8" class="ninja-forms-field nf-element" type="text" value="">
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-9-container" class="nf-field-container lastname-container  label-above half ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-9-wrap" class="field-wrap lastname-wrap" data-field-id="9">
		
		
		
	<div class="nf-field-label"><label for="nf-field-9" class="">Last Name  </label></div>

		
		<div class="nf-field-element">
    <input id="nf-field-9" name="nf-field-9" class="ninja-forms-field nf-element" type="text" value="">
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-10-container" class="nf-field-container email-container  label-above full ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-10-wrap" class="field-wrap email-wrap" data-field-id="10">
		
		
		
	<div class="nf-field-label"><label for="nf-field-10" class="">Email Address <span class="ninja-forms-req-symbol">*</span> </label></div>

		
		<div class="nf-field-element">
	<input id="nf-field-10" name="nf-field-10" class="ninja-forms-field nf-element" type="email" value="">
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-11-container" class="nf-field-container address-container  label-above full ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-11-wrap" class="field-wrap address-wrap" data-field-id="11">
		
		
		
	<div class="nf-field-label"><label for="nf-field-11" class="">Address  </label></div>

		
		<div class="nf-field-element">
	<input id="nf-field-11" name="nf-field-11" class="ninja-forms-field nf-element" type="text" value="">
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-12-container" class="nf-field-container city-container  label-above city ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-12-wrap" class="field-wrap city-wrap" data-field-id="12">
		
		
		
	<div class="nf-field-label"><label for="nf-field-12" class="">City  </label></div>

		
		<div class="nf-field-element">
	<input id="nf-field-12" name="nf-field-12" class="ninja-forms-field nf-element" type="text" value="">
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-13-container" class="nf-field-container liststate-container  label-above state ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-13-wrap" class="field-wrap liststate-wrap list-select-wrap" data-field-id="13">
		
		
		
	<div class="nf-field-label"><label for="nf-field-13" class="">State  </label></div>

		
		<div class="nf-field-element">
    <select id="nf-field-13" name="nf-field-13" class="ninja-forms-field nf-element">
        
	
	<option value="AL" selected="selected">Alabama</option>

	
	<option value="AK">Alaska</option>

	
	<option value="AZ">Arizona</option>

	
	<option value="AR">Arkansas</option>

	
	<option value="CA">California</option>

	
	<option value="CO">Colorado</option>

	
	<option value="CT">Connecticut</option>

	
	<option value="DE">Delaware</option>

	
	<option value="FL">Florida</option>

	
	<option value="GA">Georgia</option>

	
	<option value="HI">Hawaii</option>

	
	<option value="ID">Idaho</option>

	
	<option value="IL">Illinois</option>

	
	<option value="IN">Indiana</option>

	
	<option value="IA">Iowa</option>

	
	<option value="KS">Kansas</option>

	
	<option value="KY">Kentucky</option>

	
	<option value="LA">Louisiana</option>

	
	<option value="ME">Maine</option>

	
	<option value="MD">Maryland</option>

	
	<option value="MA">Massachusetts</option>

	
	<option value="MI">Michigan</option>

	
	<option value="MN">Minnesota</option>

	
	<option value="MS">Mississippi</option>

	
	<option value="MO">Missouri</option>

	
	<option value="MT">Montana</option>

	
	<option value="NE">Nebraska</option>

	
	<option value="NV">Nevada</option>

	
	<option value="NH">New Hampshire</option>

	
	<option value="NJ">New Jersey</option>

	
	<option value="NM">New Mexico</option>

	
	<option value="NY">New York</option>

	
	<option value="NC">North Carolina</option>

	
	<option value="ND">North Dakota</option>

	
	<option value="OH">Ohio</option>

	
	<option value="OK">Oklahoma</option>

	
	<option value="OR">Oregon</option>

	
	<option value="PA">Pennsylvania</option>

	
	<option value="RI">Rhode Island</option>

	
	<option value="SC">South Carolina</option>

	
	<option value="SD">South Dakota</option>

	
	<option value="TN">Tennessee</option>

	
	<option value="TX">Texas</option>

	
	<option value="UT">Utah</option>

	
	<option value="VT">Vermont</option>

	
	<option value="VA">Virginia</option>

	
	<option value="WA">Washington</option>

	
	<option value="WV">West Virginia</option>

	
	<option value="WI">Wisconsin</option>

	
	<option value="WY">Wyoming</option>

    </select>
    <div for="nf-field-13"></div>
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-14-container" class="nf-field-container zip-container  label-above zip ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-14-wrap" class="field-wrap zip-wrap" data-field-id="14">
		
		
		
	<div class="nf-field-label"><label for="nf-field-14" class="">Zip  </label></div>

		
		<div class="nf-field-element">
	<input id="nf-field-14" name="nf-field-14" class="ninja-forms-field nf-element" type="text" value="">
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-17-container" class="nf-field-container file_upload-container  label-above ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-17-wrap" class="field-wrap file_upload-wrap" data-field-id="17">
		
		
		
	<div class="nf-field-label"><label for="nf-field-17" class="">Upload Resume  </label></div>

		
		<div class="nf-field-element">
	<button class="btn btn-success fileinput-button">
        <span>Select Files</span>
    </button>
	<input class="nf-element" style="display: block; visibility: hidden; width: 0; height: 0;" type="file" name="files">
	<input type="hidden" class="nf-upload-nonce" value="13d93ca666">
	<div class="progress">
		<div class="progress-bar progress-bar-success animate"></div>
	</div>
	<div class="files_uploaded"></div>
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-18-container" class="nf-field-container textarea-container  label-above ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-18-wrap" class="field-wrap textarea-wrap" data-field-id="18">
		
		
		
	<div class="nf-field-label"><label for="nf-field-18" class="">Comments  </label></div>

		
		<div class="nf-field-element">
	<textarea id="nf-field-18" name="nf-field-18"></textarea>
</div>
		
		
	</div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-15-container" class="nf-field-container hidden-container  label-above ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-15-wrap" class="field-wrap hidden-wrap" data-field-id="15">
        <div class="nf-field-label"></div>
        <div class="nf-field-element">
    <input style="display: none;" id="nf-field-15" name="nf-field-15" class="ninja-forms-field nf-element" type="text" value="Service Maintenance Director (Legacy on the Bay)">
</div>
        <div class="nf-error-wrap"></div>
    </div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field><nf-field>
    <div id="nf-field-16-container" class="nf-field-container submit-container  label-above ">
        <div class="nf-before-field"><nf-section>
    
</nf-section></div>
        <div class="nf-field"><div id="nf-field-16-wrap" class="field-wrap submit-wrap" data-field-id="16">
        <div class="nf-field-label"></div>
        <div class="nf-field-element">
	<input id="nf-field-16" class="ninja-forms-field nf-element " type="button" value="Apply Now">
</div>
        <div class="nf-error-wrap"></div>
    </div></div>
        <div class="nf-after-field"><nf-section>
	
	<div class="nf-input-limit"></div>
	
	<div class="nf-error-wrap nf-error"></div>
	
    
</nf-section></div>
    </div>
</nf-field></nf-fields-wrap></div>
		<div class="nf-after-form-content"><nf-section>
    
    <div class="nf-form-errors"><nf-errors></nf-errors></div>
    <div class="nf-form-hp"><nf-section>
	<label for="nf-field-hp-2">
		
		<input id="nf-field-hp-2" name="nf-field-hp" class="nf-element nf-field-hp" type="text" value="">
	</label>
</nf-section></div>
</nf-section></div>
	</div></div>
		<div class="nf-after-form"><nf-section>
	
</nf-section></div>
	</div>
</div>
        

        								</div>
								
								<div class="clearfix"></div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--end sections_group-->

	</div><!--end content_wrapper-->
</div><!--end Content-->


<?php include('footer.php'); ?>