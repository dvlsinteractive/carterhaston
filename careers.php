<?php
	$page = "careers";
	include('header.php');
?>



        
<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content">


				<div class="section" id="image flv_sections_16">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">

							<!-- One full width row-->
							<div class="column one column_column" style="margin-top: 20px;">
								<div class="column_attr"><h2>Careers</h2></div>
							</div>
							
							<!-- One Third (1/3) Column -->
							<div class="column one-third column_image">
								<div class="image_frame scale-with-grid has_border aligncenter">
									<div class="image_wrapper">
										<a href="#" class="">
											<div class="mask"></div>
											<img class="scale-with-grid" src="images/careers-multi.jpg" alt="" />
										</a>
										
										<div class="image_links double">
											<a href="images/careers-multi.jpg" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="#" class="link"><i class="icon-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Two Third (2/3) Column -->
							<div class="column two-third column_column">
								<div class="column_attr ">
									<p>Carter-Haston places the highest emphasis on our culture of taking care of people. Our culture begins with a commitment to care for each other and is the foundation for developing positive relationships. We believe our success has been built upon our responsiveness to these relationships and our ability to act upon them with integrity. To help guide us towards this goal, all Carter-Haston team members follow a management philosophy based on “Five Basic Fundamentals of Successful Property Management.”</p>
								</div>
							</div>
							
							<!-- One full width row-->
							<div class="column one column_divider">
								<hr class="no_line" />
							</div>

							<!-- One full width row-->
							<div class="column one column_column" style="margin-top: 20px;">
								<div class="column_attr"><h3 class="themecolor">Five basic Fundamentals of Successful Property Management</h3></div>
							</div>
							
							<!-- One Third (1/3) Column -->
							<div class="column one-third column_list">
								<div class="list_item lists_4 clearfix">
									<div class="circle">1</div>
									<div class="list_right">
										<div class="desc">Community Appearance</div>
									</div>
								</div>
							</div>
							
							<!-- One Third (1/3) Column -->
							<div class="column one-third column_list">
								<div class="list_item lists_4 clearfix">
									<div class="circle">2</div>
									<div class="list_right">
										<div class="desc">Appearance and Attitude of Management Team</div>
									</div>
								</div>
							</div>

							<!-- One Third (1/3) Column -->
							<div class="column one-third column_list">
								<div class="list_item lists_4 clearfix">
									<div class="circle">3</div>
									<div class="list_right">
										<div class="desc">Market-Ready Apartment Homes</div>
									</div>
								</div>
							</div>   


							<!-- One full width row-->
							<div class="column one column_divider">
								<hr class="no_line" />
							</div>
							
							<!-- One Third (1/3) Column -->
							<div class="column one-third column_list">
								<div class="list_item lists_4 clearfix">
									<div class="circle">4</div>
									<div class="list_right">
										<div class="desc">Prompt Attention to Service Requests</div>
									</div>
								</div>
							</div>

							<!-- One Third (1/3) Column -->
							<div class="column one-third column_list">
								<div class="list_item lists_4 clearfix">
									<div class="circle">5</div>
									<div class="list_right">
										<div class="desc">Resident Satisfaction</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section" id="lists flv_sections_16">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">

							<div class="column mcb-column one column_column  ">
								<div class="column_attr"><h3>Benefits Overview</h2></div>
							</div>

							<div class="column mcb-column one-second column_column  ">
								<div class="column_attr">
									<p>Carter-Haston believes our employees are our most valuable asset and are essential to our continued success. This is why we proudly offer a comprehensive and valuable benefits package designed to fit the need of our employees and their families.</p>
									<p>We offer eligible associates the following:</p>
										<ul>
											<li>Competitive Compensation</li>
											<li>Paid Vacation, Sick Days, Holidays and Personal Day</li>
											<li>Medical, Dental and Vision Coverage</li>
											<li>Apartment Discount Program</li>
											<li>Recognition and Service Awards Program</li>
											<li>Educational Assistance Program</li>
											<li>Associate Discount Program</li>
											<li>Employee Assistance Program</li>
											<li>Associate Career Development</li>
										</ul>
									</p>
								</div>
							</div>
							
							<div class="column mcb-column one-second column_column  ">
								<div class="column_attr">
									<p>At Carter-Haston, we are committed to the development of our associates and understand that learning is an ongoing process. Our career development programs are designed to provide our associates a clear path to a successful and rewarding career within both Carter-Haston and the broader multifamily industry. This approach has enabled Carter-Haston to maintain a talented workforce that is respected within our industry.</p>
									<p>Carter-Haston's extensive Awards & Recognition Program recognizes exemplary performance by its associates. In addition, we honor the tenure of our associates by awarding additional benefits at various service anniversary milestones.</p>
									<h4 class="themecolor">Special Functions</h4>
									<p>During each year, members of the Carter-Haston family come together for a variety of functions. These are truly times for our associates to have some fun, learn, engage one another and celebrate our successes.
Carter-Haston is an Equal Opportunity Employer and a Drug Free Workplace.</p>
								</div>
							</div>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div>
       
<?php include('footer.php'); ?>