	<!-- Footer-->        
	<footer id="Footer" class="clearfix">
		<div class="widgets_wrapper" style="padding:60px 0;">
			<div class="container">
				
				<!-- start column -->
				<div class="column one-third">
					<aside id="text-2" class="widget widget_text">
						
						<div class="textwidget">
							<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
								<div class="image_wrapper"><img class="scale-with-grid" src="images/CHLogoREV.png" alt="Carter Haston Logo" width="225" height="25" /></div>
							</div>
							
							<hr class="no_line" style="margin: 0 auto 20px;" />
							<p style="margin-right: 10%;">Carter-Haston is a private fund management and property management company that focuses on maximizing return on capital invested while managing risk for its investors through the acquisition, management and sale of multifamily communities located in the United States.</p>
							<a href="#">Read more ...</a>
						</div>
					</aside>
				</div>
				<!-- end column -->
				
				<!-- start column -->
				<div class="column one-third">
					<aside id="text-3" class="widget widget_text">
						<div class="textwidget footerNav">
							<ul>
								<li><a href="#">Investor Login</a></li>
								<li><a href="#">Employee Login</a></li>
								<li><a href="#">Investors</a></li>
								<li><a href="#">Communities</a></li>
								<li><a href="#">News &amp; Blog</a></li>
								<li><a href="#">Careers &amp; Vendors</a></li>
							</ul>
						</div>
					</aside>
				</div>
				<!-- end column -->
				
				<!-- start column -->
				<div class="column one-third">
					<aside id="text-4" class="widget widget_text">
						<h4>Contact Carter-Haston</h4>
						<div class="textwidget">
							<p>We would love to hear from you. Please visit our <a href="#">contact page</a> to best connect with a member of the Carter-Haston team.</p>
							<div class="footerSocialLinks">
								<a style="color: #8dc63f;" href="#"><i class="icon-facebook-circled"></i></a>
								<a style="color: #8dc63f;" href="#"><i class="icon-twitter-circled"></i></a>
								<a style="color: #8dc63f;" href="#"><i class="icon-linkedin-circled"></i></a>
							</div>
							<p>Contact us: 615-812-1176 <br>Write us: <a href="mailto:#">info@carterhaston.com</a></p>
						</div>
						<div class="awardLogo">
							<img src="images/iconBPW-white.png" />
						</div>
					</aside>
				</div>
				<!-- end column -->
			</div>
		</div>
		
		<div class="footer_copy">
			<div class="container">
				<div class="column one">
					<div class="copyright">&copy; <?php echo date('Y'); ?> Carter-Haston</div>
					<ul class="social"></ul>
				</div>
			</div>
		</div>
	</footer>

</div><!--end #Wrapper-->


<!-- global js -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="js/combined.js"></script>
<script type="text/javascript" src="js/functions.js"></script>


<!-- include google map settings -->
<?php if($map == true) { include('inc_mapScript.php'); } ?>



<script id="mfn-dnmc-retina-js">
	jQuery(window).load(function() {
	  var retina = window.devicePixelRatio > 1 ? true : false;
	  if (retina) {
	      var retinaEl = jQuery("#logo img.logo-main");
	      var retinaLogoW = retinaEl.width();
	      var retinaLogoH = retinaEl.height();
	      retinaEl.attr("src", "content/architect2/images/retina-architect2.png").width(retinaLogoW).height(retinaLogoH);
	      var stickyEl = jQuery("#logo img.logo-sticky");
	      var stickyLogoW = stickyEl.width();
	      var stickyLogoH = stickyEl.height();
	      stickyEl.attr("src", "content/architect2/images/retina-architect2.png").width(stickyLogoW).height(stickyLogoH);
	      var mobileEl = jQuery("#logo img.logo-mobile");
	      var mobileLogoW = mobileEl.width();
	      var mobileLogoH = mobileEl.height();
	      mobileEl.attr("src", "content/architect2/images/retina-architect2.png").width(mobileLogoW).height(mobileLogoH);
	  }
	});
</script>



</body>
</html>