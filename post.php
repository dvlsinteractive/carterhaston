<?php
	$page = "post";
	include('header.php');
?>


<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div id="post-175" class="post-175 post type-post status-publish format-standard has-post-thumbnail hentry category-motion category-photography category-uncategorized tag-eclipse tag-grid tag-mysql no-title">
				<div class="section section-post-header">
					<div class="section_wrapper clearfix">
					
					<!-- Posts Navigation -->
					<div class="column one post-nav">
						<ul class="next-prev-nav">
							<li class="prev">
								<a class="button button_js" href="blog-single-vertical-photo.html"><span class="button_icon"><i class="icon-left-open"></i></span></a>
							</li>
						</ul>

						<a class="list-nav" href="#"><i class="icon-layout"></i>Show all</a>
					</div>

					<!-- Post Header-->
					<div class="column one post-header">
						<div class="title_wrapper">
							<div class="post-meta clearfix">
								<div class="author-date">
									<span class="author">Published by <i class="icon-user"></i> <a href="#">Amanda Speed</a></span> 
									<span class="date">at <i class="icon-clock"></i><time class="entry-date" datetime="2014-05-13T15:40:00+00:00" >Dec 11, 2016</time></span>
								</div>

								<div class="category meta-categories">
									<span class="cat-btn">Categories <i class="icon-down-dir"></i></span>

									<div class="cat-wrapper">
										<ul class="post-categories">
											<li><a href="category-page.html" rel="category tag">Investment</a></li>
											<li><a href="category-page.html" rel="category tag">Management</a></li>
											<li><a href="category-page.html" rel="category tag">Acquisitions</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Post Featured Element (image / video / gallery)-->
					<div class="column one single-photo-wrapper">

						<div class="image_frame scale-with-grid">
							<div class="image_wrapper">
								<a href="images/beauty_portfolio_2.jpg" rel="prettyphoto">
									<div class="mask"></div>
									<img width="1200" height="480" src="images/articlepost.jpg" class="scale-with-grid wp-post-image" alt="beauty_portfolio_2" />
								</a>
							
								<div class="image_links">
									<a href="images/beauty_portfolio_2.jpg" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		
		
			<!-- Post Content-->
			<div class="post-wrapper-content">
				<div class="entry-content">
					<div class="section flv_sections_11">
						<div class="section_wrapper clearfix">
							<div class="items_group clearfix">
							
							<!-- One full width row-->
							<div class="column one column_column">
								<div class="column_attr ">
									<h1>Investment Appreciation</h1>
									<p class="flv_style_4">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien <a href="#">ullamcorper pharetra</a>. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum.</p>
								</div>
							</div>

							<!-- One full width row-->
							<div class="column one column_column">
								<div class="column_attr ">
									<p class="flv_style_4">Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor.</p>
									</div>
								</div>

								<!-- One full width row-->
								<div class="column one column_faq">
									<div class="faq">
										<div class="mfn-acc faq_wrapper open1st">
											<div class="question">
												<div class="title">
													<span class="num">1</span><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i>Sed est elit posuere ac semper at hendrerit a neque?
												</div>
												
												<div class="answer">Lorem ipsum dolor sit amet tempor ac, laoreet feugiat. Proin id dui. Integer a placerat at, mollis nunc vel neque sollicitudin augue sit amet magna. Donec aliquam dictum quis, tincidunt molestie, neque nibh ultricies nec, aliquam purus. Fusce convallis non, facilisis sodales. Vivamus sem at augue. Nulla et magnis dis parturient montes, nascetur ridiculus mus. Vivamus justo.</div>
											</div>
											
											<div class="question">
												<div class="title">
													<span class="num">2</span><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i>Donec vestibulum justo a diam ultricies pellentesque?
												</div>

												<div class="answer">
													<p><span class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem.</span></p>
													<p>Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. In viverra ipsum ac eros tristique dignissim. Donec aliquam velit vitae mi dictum.</p>
												</div>
											</div>

											<div class="question">
												<div class="title">
													<span class="num">3</span><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i>Quisque mattis diam vel lacus tincidunt elementum?
												</div>
												
												<div class="answer">Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. In viverra ipsum ac eros tristique dignissim. Donec aliquam velit vitae mi dictum.</div>
											</div>

											<div class="question">
												<div class="title">
													<span class="num">4</span><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i>Integer a placerat at, mollis nunc vel neque sollicitudin?
													</div>
													
													<div class="answer">Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. In viverra ipsum ac eros tristique dignissim. Donec aliquam velit vitae mi dictum.</div>
												</div>
											</div>
										</div>
									</div>
									
									<!-- One full width row-->
									<div class="column one column_column">
										<div class="column_attr ">
										<p>Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</p>

										<p>Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
										
										<div class="hr_dots"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section section-post-footer">
					<div class="section_wrapper clearfix">
					
					<!-- One full width row-->
					<div class="column one post-pager"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


<?php include('footer.php'); ?>