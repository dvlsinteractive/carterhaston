<?php
	$page = "support-staff";
	include('header.php');
?>




<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<div class="section mcb-section   " style="padding-top:0px; padding-bottom:50px; ">
					
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
						
							<!-- One full width row-->
							<div class="column one column_fancy_heading">
								<div class="fancy_heading" style="padding-top:80px"><h2 class="title">Support Staff</h2></div>
								
								<div class="column mcb-column one column_divider ">
									<hr class="" style="margin: 0 auto 70px;" />
								</div>
							</div>
						</div>
						
						
						<div class="wrap mcb-wrap one-fourth  valign-top clearfix" style="padding:0 1%">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_image ">
									<div class="image_frame image_item scale-with-grid aligncenter no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid noZoom" src="images/Susie-Conley.jpg" alt="Susie Conley" />
										</div>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr" style=" padding:0 10% 0 0;">
										<h4 style="margin-left: 30px;">Susie Conley</h4>
										<p class="subtitle" style="margin-left: 30px;">Human Resources Manager</p>
                    <hr class="no_line" style="margin: 0 auto 20px;" />
										<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3" />
										</div>
									</div>
									<hr class="no_line" style="margin: 0 auto 20px;" />
								</div>
							</div>
						</div>
					</div>
					
					

					<div class="wrap mcb-wrap one-fourth  valign-top clearfix" style="padding:0 1%">
						<div class="mcb-wrap-inner">
							<div class="column mcb-column one column_image ">
								<div class="image_frame image_item scale-with-grid aligncenter no_border">
									<div class="image_wrapper">
										<a href="#_">
											<div class="mask"></div>
											<img class="scale-with-grid" src="images/Nelda-Harrison.jpg" alt="Nelda HarrisonDo" width="780" height="880" />
										</a>
										
										<div class="image_links ">
											<a href="#_" class="link"><i class="icon-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							
							<div class="column mcb-column one column_column  ">
								<div class="column_attr" style=" padding:0 10% 0 0;">
									<h4 style="margin-left: 30px;">Nelda Harrison</h4>
									<p class="subtitle" style="margin-left: 30px;">Operations Support</p> 
									<hr class="no_line" style="margin: 0 auto 20px;" />
									<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
									<div class="image_wrapper">
										<img class="scale-with-grid" src="images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3" />
									</div>
								</div>
								<hr class="no_line" style="margin: 0 auto 20px;" />
							</div>
						</div>
					</div>
				</div>

				<div class="wrap mcb-wrap one-fourth  valign-top clearfix" style="padding:0 1%">
					<div class="mcb-wrap-inner">
						<div class="column mcb-column one column_image ">
							<div class="image_frame image_item scale-with-grid aligncenter no_border">
								<div class="image_wrapper">
									<a href="#_">
										<div class="mask"></div>
										<img class="scale-with-grid" src="images/Donald-Evans.jpg" alt="Donald Evans" width="780" height="880" />
									</a>
									
									<div class="image_links ">
										<a href="#_" class="link"><i class="icon-link"></i></a>
									</div>
								</div>
							</div>
						</div>

						<div class="column mcb-column one column_column  ">
							<div class="column_attr" style=" padding:0 10% 0 0;">
								<h4 style="margin-left: 30px;">Donald Evans</h4>
								<p class="subtitle" style="margin-left: 30px;">Safety &amp; Compliance Officer</p> 
								<hr class="no_line" style="margin: 0 auto 20px;" />
								
								<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
									<div class="image_wrapper">
										<img class="scale-with-grid" src="images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3" />
									</div>
								</div>
								<hr class="no_line" style="margin: 0 auto 20px;" />
							</div>
						</div>
					</div>
				</div>

				<div class="wrap mcb-wrap one-fourth  valign-top clearfix" style="padding:0 1%">
					<div class="mcb-wrap-inner">
						<div class="column mcb-column one column_image ">
							<div class="image_frame image_item scale-with-grid aligncenter no_border">
								<div class="image_wrapper">
									<a href="#_">
										<div class="mask"></div>
										<img class="scale-with-grid" src="images/Katie-Moore.jpg" alt="Katy Moore" width="780" height="880" />
									</a>
									
									<div class="image_links ">
										<a href="#_" class="link"><i class="icon-link"></i></a>
									</div>
								</div>
							</div>
						</div>

						<div class="column mcb-column one column_column  ">
							<div class="column_attr" style=" padding:0 10% 0 0;">
								<h4 style="margin-left: 30px;">Katy Moore</h4>
								<p class="subtitle" style="margin-left: 30px;">Property Accountant</p>
								<hr class="no_line" style="margin: 0 auto 20px;" />
								
								<div class="image_frame image_item no_link scale-with-grid alignnone no_border">
									<div class="image_wrapper">
										<img class="scale-with-grid" src="images/home_architect2_sep.png" alt="home_architect2_sep" width="100" height="3" />
									</div>
								</div>
								<hr class="no_line" style="margin: 0 auto 20px;" />
							</div>
						</div>
					</div>
				</div>


				
			</div>
		</div>
	</div>
</div>
</div>
</div>
       

<?php include('footer.php'); ?>