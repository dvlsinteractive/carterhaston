<div class="mfn-main-slider">
	<div class="carousel">
		<div class="slide" style="background:url('http://23.235.205.152/~chadmin/site/web/assets/2017/01/Carousel.jpg');">
			<div class="text">
				<p><strong>Experience</strong></p>
				<p>Over its 30-year history, the firm has acquired and managed nearly 100 apartment communities with an asset value over $2 billion.</p>
			</div>
			
			<div class="tag">
				<p>Brookleigh Flats</p>
			</div>
		</div>
		
		<div class="slide" style="background:url('images/home_architect2_slider2-1200x500.jpg');"></div>
		
	</div>
	<div class="carousel-nav">
		<a href="javascript:void(0);" class="prev">
			<img src="images/arrow-left.svg" />
		</a>
		<div class="dots"></div>
		<a href="javascript:void(0);" class="next">
			<img src="images/arrow-right.svg" />
		</a>
	</div>
</div>
