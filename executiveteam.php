<?php
	$page = "executive-team";
	include('header.php');
?>


<div id="Content">
	<div class="content_wrapper clearfix">
		
		<div class="sections_group">
			<div class="entry-content">
				<div class="section flv_sections_11">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
						
						
						<!-- Page Title-->
						<div class="column one column_fancy_heading">
							<div class="fancy_heading" style="padding-top:80px">
								<h2 class="title">Executive Team</h2>
							</div>
							
							<div class="column mcb-column one column_divider ">
								<hr class="" style="margin: 0 auto 70px;" />
							</div>
						</div>



						<!-- start row -->
						<div class="column one column_our_team_list">
						
							<!-- Team Member Area -->
							<div class="team team_list clearfix">
								
								<!-- start column -->
								<div class="column one-fourth">
									<div class="image_frame no_link scale-with-grid">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="images/Marc-Carter.jpg" alt="Marc Carter" />
										</div>
									</div>
								</div>
								<!-- end column -->
								
								<!-- start column -->
								<div class="column one-second">
									<div class="desc_wrapper">
										<h4>L. Marc Carter</h4>
										<p class="subtitle">CPM, Principal</p>
										<hr class="hr_color" />
										
										<div class="desc">
											<p>Marc is actively engaged in all aspects of the company with a particular focus on investment strategy, acquisitions and dispositions, and oversight of the property management platform.  Marc is co-chair of the Investment Committee with Harris.</p>

											<p>Before founding Carter-Haston in 1987, Marc was president of Jacques-Miller Property Management from 1984 to 1987, a multi-faceted real estate investment firm with more than 22,000 units owned across 22 states and 56 cites.  Prior to Jacques-Miller, Marc headed management of the 10,000 unit portfolio of Towne Properties in Cincinnati, Ohio.  He began his career in real estate in 1972 with Multicon Properties in Columbus, Ohio.</p> 

											<p>Marc is a Certified Property Manager, has sat on numerous civic and professional boards, is a director on the board of the National Multifamily Housing Council, and is a member of the National Apartment Association’s Executive Forum.  He attended Indiana University prior to graduating from Engineering Officer Candidate School.  He was honorably discharged in 1972.</p>
										</div>
									</div>
								</div>
								<!-- end column -->
								
								<!-- start column -->
								<div class="column one-fourth">
									<div class="bq_wrapper">
										<h4>Articles &amp; News</h4>
										<p>
											<ul>
												<li><a href="#">Article Headline One</a></li>
												<li><a href="#">Second Article Headline</a></li>
												<li><a href="#">Third Article Headline</a></li>
											</ul>
										</p>

										<div class="links">
											<a href="mailto:" class="icon_bar icon_bar_small">
												<span class="t"><i class="icon-mail"></i></span>
												<span class="b"><i class="icon-mail"></i></span>
											</a>
											
											<a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small">
												<span class="t"><i class="icon-facebook"></i></span>
												<span class="b"><i class="icon-facebook"></i></span>
											</a>
											
											<a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small">
												<span class="t"><i class="icon-twitter"></i></span>
												<span class="b"><i class="icon-twitter"></i></span>
											</a>
											
											<a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small">
												<span class="t"><i class="icon-linkedin"></i></span>
												<span class="b"><i class="icon-linkedin"></i></span>
											</a>
										</div>
									</div>
								</div>
								<!-- end column -->
							</div>
						</div>
						<!-- end row -->
						
						
						
						
						
						<!-- divider -->
						<div class="column one column_divider">
							<div class="hr_dots flv_margin_40">
								<span></span><span></span><span></span>
							</div>
						</div>
						
						
						
						
						
						
						
						
						<!-- start row -->
						<div class="column one column_our_team_list">
						
							<!-- Team Member Area -->
							<div class="team team_list clearfix">
							
								<!-- start column -->
								<div class="column one-fourth">
									<div class="image_frame no_link scale-with-grid">
										<div class="image_wrapper">
											<img class="scale-with-grid" src="images/Harris-Haston.jpg" alt="Harris Haston" />
										</div>
									</div>
								</div>
								<!-- end column -->
								
								<!-- start column -->
								<div class="column one-second">
									<div class="desc_wrapper">
										<h4>C. Harris Haston</h4>
										<p class="subtitle">Principal</p>
										<hr class="hr_color" />
										
										<div class="desc">
											<p>Harris is actively engaged in all aspects of the company with a particular focus on investment strategy and oversight of corporate finance activities, debt and equity structures and investor relations.  Harris is co-chair of the Investment Committee with Marc. </p>

											<p>Before founding Carter-Haston in 1987, Harris was president of Haston & Associates and Haston Real Estate, which provided development-consulting services to financial service firms and other institutional investors, and developed income-producing property for its own account.  He began his career in real estate in 1976 with First American National Bank, where his primary responsibility was real estate workouts from the 1974-1976 real estate recession. </p>

											<p>Harris earned a B.S. with a major in Finance from The University of Tennessee, Knoxville.  He is currently a director on the board of the National Multifamily Housing Council, is a member of the Rotary Club of Nashville, where he was also treasurer, is an executive board member of the Middle Tennessee Council of Boy Scouts, and is a trustee board member of the Buffalo Bill Center of the West in Cody, Wyoming.</p>
											</div>
										</div>
									</div>
									<!-- end column -->
									
									
									<!-- start column -->
									<div class="column one-fourth">
										<div class="bq_wrapper">
											<h4>Articles &amp; News</h4>
											<p>
												<ul>
													<li><a href="#">Article Headline One</a></li>
													<li><a href="#">Second Article Headline</a></li>
													<li><a href="#">Third Article Headline</a></li>
												</ul>
											</p>
											
											<div class="links">
												<a href="mailto:" class="icon_bar icon_bar_small">
													<span class="t"><i class="icon-mail"></i></span>
													<span class="b"><i class="icon-mail"></i></span>
												</a>
												
												<a target="_blank" href="http://facebook.com/" class="icon_bar icon_bar_small">
													<span class="t"><i class="icon-facebook"></i></span>
													<span class="b"><i class="icon-facebook"></i></span>
												</a>
												
												<a target="_blank" href="http://twitter.com/" class="icon_bar icon_bar_small">
													<span class="t"><i class="icon-twitter"></i></span>
													<span class="b"><i class="icon-twitter"></i></span>
												</a>
												
												<a target="_blank" href="http://www.linkedin.com/" class="icon_bar icon_bar_small">
													<span class="t"><i class="icon-linkedin"></i></span>
													<span class="b"><i class="icon-linkedin"></i></span>
												</a>
											</div>
										</div>
									</div>
									<!-- end column -->
								</div>
							</div>
							<!-- end row -->
							
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
							
							
							
							
							
							
							
							
							

<?php include('footer.php'); ?>