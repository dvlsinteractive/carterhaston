<?php
	$page = "executive-team";
	include('header.php');
?>

<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content">


				<div class="section" id="image flv_sections_16">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">

							<!-- One full width row-->
							<div class="column one column_column mt40 mb10">
								<div class="column_attr"><h2>Page Title</h2></div>
							</div>
							
							
							<div class="column one column_column">
								<div class="column_attr ">
									<p>Page Content Goes Here</p>
								</div>
							</div>
							
							<?php include('inc_open-positions.php'); ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>