<?php
	$page = "single-location";
	include('header.php');
?>


<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<div class="section mcb-section full-width no-margin-h no-margin-v  " style="padding-top:10px; padding-bottom:20px; ">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner singleLocation">
								
								<a href="#_" class="back">Back to Communities</a>
								<span class="clearfix"></span>
								
								<div class="locationInfo">
									
									<div class="image" style="background:url('images/addison-south-tryon-lg.jpg'); background-position: center center;"></div>							

									<div class="details">										
										<h3 class="themecolor" itemprop="name">Barclay at Dunwoody</h3>
										<div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
									    <span itemprop="streetAddress">4580 Barclay Drive</span><br>
									    <span itemprop="addressLocality">Dunwoody</span>,
									    <span itemprop="addressRegion">GA</span>
									    <span itemprop="postalCode">30338</span>
									  </div>
									  <div itemprop="telephone">770.986.9094</div>
									  <a class="btn" href="http://www.barclaydunwoody.com/" itemprop="url" target="_blank">Visit Website</a>
									</div><!--end details-->
									
								</div><!--end locationInfo-->
								
								<div class="locationMap">
									map goes here
								</div>
								
								<div class="clearfix"></div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--end sections_group-->

	</div><!--end content_wrapper-->
</div><!--end Content-->


<?php include('footer.php'); ?>