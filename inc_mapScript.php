  <script>
  var jsonpoints = { "locations": [
                          { 
    "title":"Ansley Commons",
    "properties":{
      "lat": 32.983863,
      "lng": -80.088500,
      "address":"3300 Shipley Street N. ",
      "city":"Charleston",
      "state":"SC",
      "zip":"29456",
      "phone":"843.297.8316",
      "type":"community",
      "url":"http://www.ansleycommons.com/"
    }
  },
                          { 
    "title":"Anthem Cityline",
    "properties":{
      "lat": 33.002236,
      "lng": -96.701453,
      "address":"1250 State Street",
      "city":"Richardson",
      "state":"TX",
      "zip":"75082",
      "phone":"972.231.2800",
      "type":"community",
      "url":""
    }
  },
                          { 
    "title":"Arbors Harbor Town",
    "properties":{
      "lat": 35.1600782,
      "lng": -90.0552072,
      "address":"671 Harbor Edge Drive",
      "city":"Memphis",
      "state":"TN",
      "zip":"38103",
      "phone":"901.526.0322",
      "type":"community",
      "url":"http://www.arborsharbortown.com/"
    }
  },
                          { 
    "title":"Barclay at Dunwoody",
    "properties":{
      "lat": 33.9296433,
      "lng": -84.2989922,
      "address":"4580 Barclay Drive",
      "city":"Dunwoody",
      "state":"GA",
      "zip":"30338",
      "phone":"770.986.9094",
      "type":"community",
      "url":"http://www.barclaydunwoody.com/"
    }
  },
                          { 
    "title":"Bay Oaks",
    "properties":{
      "lat": 27.9182487,
      "lng": -82.4922136,
      "address":"3105 Bay Oaks Court",
      "city":"Tampa",
      "state":"FL",
      "zip":"33629",
      "phone":"813.839.8496",
      "type":"community",
      "url":"http://www.livingonbayshore.com/"
    }
  },
                          { 
    "title":"Block Lofts &#8211; not on cur site",
    "properties":{
      "lat": 33.765675,
      "lng": -84.36166,
      "address":"747 Ralph McGill Blvd NE",
      "city":"Atlanta",
      "state":"GA",
      "zip":"30312",
      "phone":"404.522.4484",
      "type":"community",
      "url":"http://www.blockloftsapartments.com/"
    }
  },
                          { 
    "title":"Brookleigh Flats",
    "properties":{
      "lat": 33.8919901,
      "lng": -84.3239851,
      "address":"3450 Blair Circle NE",
      "city":"Atlanta",
      "state":"GA",
      "zip":"30319",
      "phone":"770.451.8812",
      "type":"community",
      "url":"http://www.brookleighflats.com/"
    }
  },
                          { 
    "title":"Courtney Trace",
    "properties":{
      "lat": 27.9198283,
      "lng": -82.3324663,
      "address":"1131 Courtney Trace Drive",
      "city":"Brandon",
      "state":"FL",
      "zip":"33511",
      "phone":"813.662.5566",
      "type":"community",
      "url":"http://www.courtneytrace.com"
    }
  },
                          { 
    "title":"Cumberland Pointe Apartments",
    "properties":{
      "lat": 40.0187291,
      "lng": -85.9921096,
      "address":"15800 Navigation Way",
      "city":"Noblesville",
      "state":"IN",
      "zip":"46060",
      "phone":"317.776.1400",
      "type":"community",
      "url":"http://www.cumberlandpointeapartments.com"
    }
  },
                          { 
    "title":"Eastside Village",
    "properties":{
      "lat": 33.0187649,
      "lng": -96.6989612,
      "address":"1404 Vontess St., Sute 2119",
      "city":"Plano",
      "state":"TX",
      "zip":"75074",
      "phone":"972.943.3050",
      "type":"community",
      "url":"http://www.eastsidevillageliving.com"
    }
  },
                          { 
    "title":"Glenwood East",
    "properties":{
      "lat": 33.7437574,
      "lng": -84.3540856,
      "address":"390 Stovall St SE",
      "city":"Atlanta",
      "state":"GA",
      "zip":"30316",
      "phone":"404.627.4390",
      "type":"community",
      "url":"http://www.glenwoodeast.com/"
    }
  },
                          { 
    "title":"Grand Reserve",
    "properties":{
      "lat": 29.1588,
      "lng": -82.162294,
      "address":"3001 SW 24th Avenue",
      "city":"Ocala",
      "state":"FL",
      "zip":"34471",
      "phone":"352.854.6654",
      "type":"community",
      "url":"http://www.thegrandreserveocala.com/"
    }
  },
                          { 
    "title":"Hunters Creek",
    "properties":{
      "lat": 36.2279861,
      "lng": -86.2916789,
      "address":"100 Hunters Creek Blvd",
      "city":"Lebanon",
      "state":"TN",
      "zip":"37087",
      "phone":"615.444.4324",
      "type":"community",
      "url":"http://www.livingathunterscreek.com/"
    }
  },
                          { 
    "title":"Lane Parke",
    "properties":{
      "lat": 33.4873101,
      "lng": -86.7734689,
      "address":"1000 Lane Parke Court",
      "city":"Mountain Brook",
      "state":"AL",
      "zip":"35223",
      "phone":"205.443.4567",
      "type":"community",
      "url":"http://www.laneparkeapartments.com"
    }
  },
                          { 
    "title":"Legacy at Meridian",
    "properties":{
      "lat": 35.9099586,
      "lng": -78.9121936,
      "address":"1101 Exchange Place",
      "city":"Durham",
      "state":"NC",
      "zip":"27713",
      "phone":"919.806.1988",
      "type":"community",
      "url":"http://www.legacyatmeridian.com/"
    }
  },
                          { 
    "title":"Legacy at Wakefield",
    "properties":{
      "lat": 35.9505427,
      "lng": -78.5437291,
      "address":"14411 Calloway Gap Road",
      "city":"Raleigh",
      "state":"NC",
      "zip":"27614",
      "phone":"919.554.9002",
      "type":"community",
      "url":"http://www.legacyatwakefield.com"
    }
  },
                          { 
    "title":"Legacy on the Bay",
    "properties":{
      "lat": 30.38931,
      "lng": -86.408912,
      "address":"251 Vinings Way Blvd",
      "city":"Destin",
      "state":"FL",
      "zip":"32541",
      "phone":"850.650.7446",
      "type":"community",
      "url":"http://www.legacyonthebay.com/"
    }
  },
                          { 
    "title":"Lofts at Savannah Park &#8211; not on cur site",
    "properties":{
      "lat": 28.8045892,
      "lng": -81.3489227,
      "address":"5724 Byron Anthony Pl",
      "city":"Sanford",
      "state":"FL",
      "zip":"32771",
      "phone":"407.268.3303",
      "type":"community",
      "url":"http://loftssavannahpark.com/"
    }
  },
                          { 
    "title":"Marsh Point",
    "properties":{
      "lat": 32.544686,
      "lng": -80.759439,
      "address":"100A Marsh Point Drive",
      "city":"Hilton Head Island",
      "state":"SC",
      "zip":"29926",
      "phone":"843.681.6550",
      "type":"community",
      "url":"http://www.marshpointapartments.com/"
    }
  },
                          { 
    "title":"Preserve at Spears Creek",
    "properties":{
      "lat": 34.1183648,
      "lng": -80.845108,
      "address":"325 Spears Creek Church Rd",
      "city":"Elgin",
      "state":"SC",
      "zip":"29045",
      "phone":"803.865.1100",
      "type":"community",
      "url":"http://www.preserveatspearscreek.com/"
    }
  },
                          { 
    "title":"Preserve at Steele Creek",
    "properties":{
      "lat": 35.1441006,
      "lng": -80.9745814,
      "address":"10830 Morgan Creek Drive",
      "city":"Charlotte",
      "state":"NC",
      "zip":"28273",
      "phone":"704.588.2488",
      "type":"community",
      "url":"http://www.preserveatsteelecreek.com"
    }
  },
                          { 
    "title":"Riverhouse",
    "properties":{
      "lat": 34.763742,
      "lng": -92.3063578,
      "address":"1200 Brookwood Drive",
      "city":"Little Rock",
      "state":"AR",
      "zip":"72202",
      "phone":"501.725.8777",
      "type":"community",
      "url":"http://www.riverhouselittlerock.com"
    }
  },
                          { 
    "title":"San Miguel Del Bosque",
    "properties":{
      "lat": 35.1785391,
      "lng": -106.6670019,
      "address":"9180 Coors Blvd.",
      "city":"Albuquerque",
      "state":"NM",
      "zip":"87120",
      "phone":"505.890.0009",
      "type":"community",
      "url":"http://www.sanmiguelapts.com"
    }
  },
                          { 
    "title":"Station 92",
    "properties":{
      "lat": 34.085766,
      "lng": -84.5160718,
      "address":"10247 GA-92",
      "city":"Woodstock",
      "state":"GA",
      "zip":"30188",
      "phone":"678.500.9796",
      "type":"community",
      "url":"http://crestatlaurelwood.com/"
    }
  },
                          { 
    "title":"The Addison",
    "properties":{
      "lat": 27.91977,
      "lng": -82.329687,
      "address":"2516 Annapolis Way",
      "city":"Brandon",
      "state":"FL",
      "zip":"33511",
      "phone":"813.681.7200",
      "type":"community",
      "url":"http://www.theaddisonapartments.com"
    }
  },
                          { 
    "title":"The Addison at South Tryon",
    "properties":{
      "lat": 35.153822,
      "lng": -80.9225875,
      "address":"7000 Modern Way",
      "city":"Charlotte",
      "state":"NC",
      "zip":"28217",
      "phone":"980.237.6472",
      "type":"community",
      "url":"http://www.theaddisonatsouthtryon.com"
    }
  },
                          { 
    "title":"The Preserve at Windsor Lake",
    "properties":{
      "lat": 34.07379,
      "lng": -80.9330513,
      "address":"1460 Oakcrest Dr",
      "city":"Columbia",
      "state":"SC",
      "zip":"29223",
      "phone":"803.736.1099",
      "type":"community",
      "url":"http://www.PreserveAtWindsorLake.com"
    }
  },
                          { 
    "title":"The Q at Maitland",
    "properties":{
      "lat": 28.6255906,
      "lng": -81.3617821,
      "address":"430 E Packwood Ave",
      "city":"Maitland",
      "state":"FL",
      "zip":"32751",
      "phone":"407.644.3663",
      "type":"community",
      "url":"http://www.theqatmaitland.com/"
    }
  },
                          { 
    "title":"The Retreat at Panama City",
    "properties":{
      "lat": 30.2490768,
      "lng": -85.9278547,
      "address":"90 Sandal Lane",
      "city":"Panama City",
      "state":"FL",
      "zip":"32413",
      "phone":"850.230.0074",
      "type":"community",
      "url":"http://www.retreatpcb.com"
    }
  },
                          { 
    "title":"Tradition at Summerville",
    "properties":{
      "lat": 33.032974,
      "lng": -80.151814,
      "address":"325 Marymeade Drive",
      "city":"Summerville",
      "state":"SC",
      "zip":"29483",
      "phone":"843.821.4500",
      "type":"community",
      "url":"http://www.traditionapts.net/"
    }
  },
                          { 
    "title":"Verandahs of Brighton Bay",
    "properties":{
      "lat": 27.8709539,
      "lng": -82.6260748,
      "address":"10800 Brighton Bay Blvd NE",
      "city":"St. Petersburg",
      "state":"FL",
      "zip":"33716",
      "phone":"727.577.9800",
      "type":"community",
      "url":"http://www.theverandahsofbrightonbay.com/"
    }
  },
                          { 
    "title":"West End Village",
    "properties":{
      "lat": 36.14585,
      "lng": -86.8172126,
      "address":"221 31st Ave North",
      "city":"Nashville",
      "state":"TN",
      "zip":"37203",
      "phone":"615.383.1322",
      "type":"community",
      "url":"http://www.westendvillageapartments.com/"
    }
  },
                          { 
    "title":"Whitepalm",
    "properties":{
      "lat": 29.1108134,
      "lng": -81.0403919,
      "address":"5400 S Williamson Blvd",
      "city":"Port Orange",
      "state":"FL",
      "zip":"32128",
      "phone":"386.788.6658",
      "type":"community",
      "url":"http://www.whitepalmapts.com/"
    }
  },
]};

  $(document).ready(function() {

    //change location to the community selected
    $('#communityChange').on('change', function(){
      window.location.href=this.options[this.selectedIndex].value;
    });
    
    //filter cities based on selected state 
    $("#SearchCity").chained("#SearchState");
    
  }); //end document ready 
  
  
  
  
  
  
  
    
  //setup map variables
    var icon1 = { path: "M17.76,0a2.18,2.18,0,0,1,1.56.65A2.06,2.06,0,0,1,20,2.19V17.76A2.32,2.32,0,0,1,17.76,20H13.33L10,23.33,6.67,20H2.24a2.13,2.13,0,0,1-1.59-.68A2.17,2.17,0,0,1,0,17.76V2.19A2.08,2.08,0,0,1,.65.65,2.19,2.19,0,0,1,2.24,0Z", fillColor: '#000000', fillOpacity: .6, strokeWeight: 0, scale: 1}

    function initMap() {
      var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng((35.9099586),-78.9121936),
        zoom: 7
        });
                
        var infowindow;
          (function() {
              google.maps.Map.prototype.markers = new Array();
              google.maps.Map.prototype.addMarker = function(marker) {
                  this.markers[this.markers.length] = marker;
              };
              google.maps.Map.prototype.getMarkers = function() {
                  return this.markers
              };
              google.maps.Map.prototype.clearMarkers = function() {
                  if (infowindow) {
                      infowindow.close();
                  }
                  for (var i = 0; i < this.markers.length; i++) {
                      this.markers[i].set_map(null);
                  }
              };
          })();       
        
        var markers = [];
        
          
        function createMarker(name, latlng) {
          //custom marker
          var icon1 = { path: "M17.76,0a2.18,2.18,0,0,1,1.56.65A2.06,2.06,0,0,1,20,2.19V17.76A2.32,2.32,0,0,1,17.76,20H13.33L10,23.33,6.67,20H2.24a2.13,2.13,0,0,1-1.59-.68A2.17,2.17,0,0,1,0,17.76V2.19A2.08,2.08,0,0,1,.65.65,2.19,2.19,0,0,1,2.24,0Z", fillColor: '#000000', fillOpacity: .6, strokeWeight: 0, scale: 1};

          var marker = new google.maps.Marker({ // create new marker with options
              position: latlng,
              icon:icon1, // set icon for the marker
              map: map
          });
          
          markers.push(marker); // push the marker onto the markers array for use in creating boundaries
            
          google.maps.event.addListener(marker, "click", function() { // add click event to the markers in order to close other windows. 
              if (infowindow) infowindow.close();
              infowindow = new google.maps.InfoWindow({
                  content: name
              });
              infowindow.open(map, marker);
          });
          return marker;
        } //end createMarker
        
        //loop inpage json data
        Array.prototype.forEach.call(jsonpoints.locations, function(item) {
          var name = item.title || "";
          var address = item.properties.address || "";
          var type = item.properties.type || "";
          var website = item.properties.url || "";
          var phone = item.properties.phone || "";
          var city = item.properties.city || "";
          var state = item.properties.state || "";
          var point = new google.maps.LatLng(
              parseFloat(item.properties.lat),
              parseFloat(item.properties.lng)
              );
            
          var content = '<div class="iWindow"><div class="iwRow1 clearfix"><div class="iwTitle">'+name+'</div><div class="iwPhone">'+phone+'</div></div><div class="iwRow2 clearfix"><div class="iwAddressWrapper"><div class="iwAddress">'+address+'<br/>'+city+", "+state+'</div><div class="iwWebsite"><a href="'+website+'" target="_blank">Visit Website</a></div></div></div></div>';

          map.addMarker(createMarker(content, point));
          
          //set bounds of the map to encompass all the markers, based on the markers array populated in createMarker
          var bounds = new google.maps.LatLngBounds();
          for (var i = 0; i < markers.length; i++) {
           bounds.extend(markers[i].getPosition());
          }
          map.fitBounds(bounds);
         
        });//end json loop        
        
    } //end map callback function 
      
      
      
      
  </script>
  
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRiWQAVq8ZO30aa-grooPn0bW5kc1GLWU&callback=initMap">
    </script>
