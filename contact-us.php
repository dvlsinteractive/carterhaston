<?php
	$page = "contact";
	$map = true;
	include('header.php'); 
?>



<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<div class="section mcb-section" style="padding-top:0px; padding-bottom:20px; ">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner">
								
								<div class="column mcb-column one column_divider ">
									<hr class="" style="margin: 0 auto 70px;" />
								</div>
							</div>
						</div>
						
						<div class="wrap mcb-wrap one-second  valign-top clearfix">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_column  ">
									<div class="column_attr">
										<h3>Send us a message</h3>
										<hr class="no_line" style="margin: 0 auto 30px;" />
										
										<div id="contactWrapper">
											<form id="contactform">
												<div class="column one-second">
													<input placeholder="Your name" type="text" name="name" id="name" size="40" aria-required="true" aria-invalid="false" />
												</div>
												
												<div class="column one-second">
													<input placeholder="Your e-mail" type="email" name="email" id="email" size="40" aria-required="true" aria-invalid="false" />
												</div>
												
												<div class="column one">
													<input placeholder="Subject" type="text" name="subject" id="subject" size="40" aria-invalid="false" />
												</div>
												
												<div class="column one">
													<textarea placeholder="Message" name="body" id="body" style="width:100%;" rows="10" aria-invalid="false"></textarea>
												</div>

												<div class="column one">
													<input type="button" value="Send A Message" id="submit" onClick="return check_values();">
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="wrap mcb-wrap one-second  valign-top clearfix" style="padding:0 0 0 5%">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_column  ">
									<div class="column_attr" style=" background-image:url(images/home_architect2_contact_icon1.png); background-repeat:no-repeat; background-position:left top; background-attachment:; -webkit-background-size:; background-size:; padding:0 0 0 80px;">
										<p style="margin-bottom: 7px;">telephone: <span class="themecolor">615-812-1176</span></p>
										<p>e-mail: <a href="#">noreply@carter-haston.com</a></p>
									</div>
								</div>

								<div class="column mcb-column one column_column  ">
									<div class="column_attr" style=" background-image:url(images/home_architect2_contact_icon2.png); background-repeat:no-repeat; background-position:left top; background-attachment:; -webkit-background-size:; background-size:; padding:0 30% 0 80px;">
										<h6>Aenean sollicitudin, lorem quis bibendum auctor nisi.</h6>
										<p>Elit consequat ipsum, nec sagittis sem nibh id elit vitae justo.</p>
										<p>Carter-Haston
										<br>  3301 West End Ave #200
										<br> Nashville, TN 37203</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section mcb-section full-width no-margin-h no-margin-v  " style="padding-top:0px; padding-bottom:0px; ">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner">
								<div class="column mcb-column one column_map ">

									<div class="google-map-wrapper no_border">
										<div class="google-map" id="google-map-area-572224c8e533a" style="width:100%; height:550px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php include('footer.php'); ?>