<?php 
	$page = 'home';
	$carousel = true;
	include('header.php');
?>

    
<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<div class="section mcb-section" style="padding-top:0px; padding-bottom:0px; ">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one  valign-top clearfix">
							<div class="mcb-wrap-inner animatedNumbers">
								
								<!-- start column -->
								<div class="column mcb-column one column_column">
									<div class="column_attr align_center">
										<div style="display: inline-block; border-left: 1px solid #ccced3; text-align: left; padding: 50px 0 0 0;">
											<h2 style="margin: 10px;">Investment &amp; Management Foresight</h2><span style="display: inline-block; background: #999; color: #fff; padding: 4px 12px; font-weight: 700; letter-spacing: 3px;">BY THE NUMBERS</span>
										</div>
									</div>
								</div>
								<!-- end column -->
								
								<!-- start column -->
								<div class="column mcb-column one-fourth column_quick_fact">
									<div class="quick_fact animate-math">
										<div class="number-wrapper">
											<span class="number" data-to="25">25</span>
										</div>
										<h3 class="title">Years of Experience</h3>
										<hr class="hr_narrow" />
										<div class="desc"></div>
									</div>
								</div>
								<!-- end column -->
								
								<!-- start column -->
								<div class="column mcb-column one-fourth column_quick_fact ">
									<div class="quick_fact animate-math">
										<div class="number-wrapper">
											<span class="number" data-to="10000">10000</span>
										</div>
										<h3 class="title">Units Managed</h3>
										<hr class="hr_narrow" />
										<div class="desc"></div>
									</div>
								</div>
								<!-- end column -->
								
								<!-- start column -->
								<div class="column mcb-column one-fourth column_quick_fact ">
									<div class="quick_fact">
									<div class="number-wrapper">
										<span class="number">2 billion</span>
									</div>
									<h3 class="title">Property Value</h3>
									<hr class="hr_narrow" />
									<div class="desc"></div>
								</div>
							</div>
							<!-- end column -->
							
							<!-- start column -->
							<div class="column mcb-column one-fourth column_quick_fact ">
								<div class="quick_fact animate-math">
									<div class="number-wrapper">
										<span class="number" data-to="300">300</span>
									</div>
									<h3 class="title">Employees</h3>
									<hr class="hr_narrow" />
									<div class="desc"></div>
								</div>
							</div>
							<!-- end column -->
							
							<!-- start column -->
							<div class="column mcb-column one column_divider ">
								<hr class="" />
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- carousel start-->
			<div class="section mcb-section homeBlogCarousel">
				<div class="section_wrapper mcb-section-inner">
					<div class="wrap mcb-wrap one  valign-top clearfix">
						<div class="mcb-wrap-inner">
							
							<!-- start column -->
							<div class="column mcb-column one column_offer_thumb ">
								<div class="offer_thumb">
									<ul class="offer_thumb_ul">
										<li class="offer_thumb_li id_1">
											<div class="image_wrapper"><img width="800" height="551" src="images/home_architect2_offerslider1.jpg" class="scale-with-grid wp-post-image" alt="home_architect2_offerslider1" /></div>
											<div class="desc_wrapper">
											
												<div class="desc">
													<div style="text-align: right; margin: 0 2% 0 10%;">
														<h3 style="margin-bottom: 3px;">This Just In: Release</h3>
														<h3 class="themecolor">Units Acquisition</h3>
														<h6>Carter-Haston acquires 138 units in Orlando</h6>
														<p>Carter-Haston is pleased to announce the successful acquisition of Lofts at Savannah Park, a 138-unit community located in Sanford, Orlando MSA, FL.  Carter-Haston now owns and manages two communities in Orlando.</p>
														<p><a href="#">Read more &#8230;</a></p>
													</div>
												</div>
											</div>
										
											<div class="thumbnail" style="display:none"><img src="images/home_architect2_contentslider_icon1.jpg" class="scale-with-grid" alt="" /> </div>
										</li>
										
										<li class="offer_thumb_li id_2">
											<div class="image_wrapper"><img width="800" height="551" src="images/home_architect2_offerslider3.jpg" class="scale-with-grid wp-post-image" alt="home_architect2_offerslider2" /> </div>
											<div class="desc_wrapper">
												<div class="desc">
													<div style="text-align: right; margin: 0 2% 0 10%;">
														<h3 style="margin-bottom: 3px;">This Just In: Blog</h3>
														<h3 class="themecolor">Investment Appreciation</h3>
														<h6>By: James A. Shanks</h6>
														<p>Nulla consectetuer, augue sit amet interdum euismod pulvinar, libero. Donec pulvinar sed, tempus ut, dignissim faucibus, quam. </p>
														<p><a href="#">Read more &#8230;</a></p> 
													</div>
												</div>
											</div>
											
											<div class="thumbnail" style="display:none"><img src="images/home_architect2_contentslider_icon2.jpg" class="scale-with-grid" alt="" /> </div>
										</li>
										
										<li class="offer_thumb_li id_3">
											<div class="image_wrapper"><img width="800" height="551" src="images/home_architect2_offerslider2.jpg" class="scale-with-grid wp-post-image" alt="home_architect2_offerslider3" /> </div>
											<div class="desc_wrapper">
												<div class="desc">
													<div style="text-align: right; margin: 0 2% 0 10%;">
														<h3 style="margin-bottom: 3px;">This Just In: Release</h3>
														<h3 class="themecolor">Brookleigh Flats</h3>
														<h6>Demand for rental properties is on the rise</h6>
														<p>Etiam nunc faucibus orci luctus euismod. Integer id nonummy id, placerat vehicula ut, semper eros. Aliquam gravida sagittis. Nullam fermentum erat. </p>
														<p><a href="#">Read more &#8230;</a></p> 
													</div>
												</div>
											</div>
											
											<div class="thumbnail" style="display:none"><img src="images/home_architect2_contentslider_icon3.jpg" class="scale-with-grid" alt="" /> </div>
										</li>
									</ul>
									
									<div class="slider_pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="homeNewsFeature">
				<div class="section_wrapper mcb-section-inner">
					<div class="wrap mcb-wrap one  valign-top clearfix">
						<div class="mcb-wrap-inner">
							<div class="content">
								<div class="image">
									<img src="images/CH-Doing-Good-Green.jpg" />
							</div>
							<div class="text">
								<p>Beginning in 2017, we are designating the month of May for philanthropic activity company wide. Our campaign is Doing Good in our Neighborhood. We have philanthropic campaigns throughout the year that include Toys for Tots, local food drives, Breast Cancer Awareness month, etc., but May is an opportunity to come together as a team and physically do good in our neighborhood. This year, activities ranged from renovating an apartment unit at a shelter to building an entrance ramp to the home of a veteran to clean-up and repairs at an animal shelter to sorting and boxing meals for delivery to families in need. Thanks to all of our team members for making this year’s Doing Good in our Neighborhood a success.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div><!--end Content-->
         

<?php include('footer.php'); ?>